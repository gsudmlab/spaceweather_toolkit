# swdatatoolkit


* [INFO](#markdown-header-info)
* [TEST BUILD](#markdown-header-test-build)

***
***

## INFO

The Space Weather Toolkit project is a library that is utilized by the [Data Mining Lab](http://dmlab.cs.gsu.edu/) of 
[Georgia State University](http://www.gsu.edu/) for several projects that have shared requirements.

***

## TEST Build

The build section is only for building and running on for local testing while developing. 

If you wish to continue to run just the local project, continue to [TEST_BUILD.md](TEST_BUILD.md)

***

## Acknowledgment

This project has been supported in part by funding from the Division of Advanced Cyberinfrastructure within the 
Directorate for Computer and Information Science and Engineering, the Division of Atmospheric & Geospace Sciences 
within the Directorate for Geosciences, under NSF awards #1931555 and #1936361. It has also been supported by NASA's 
Space Weather Science Application Research-to-Operations-to-Research program grant #80NSSC22K0272, Multidomain 
Reusable Artificial Intelligence Tools program grant #80NSSC23K1026, and Heliophysics Artificial Intelligence/Machine 
Learning-ready Data program grant #80NSSC24K0238 .

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)  
![alt text](./images/gplv3-88x31.png) 

This software is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
General Public License for more details.

***

© 2025 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)