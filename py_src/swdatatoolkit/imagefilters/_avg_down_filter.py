"""
 * swdatatoolkit, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2024 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy


class AverageDownSampleFilter:
    """
    Class that reduces the size of an input array by providing the average of the cells that were combined to produce
    the reduced array.
    """

    def __init__(self, sacale: float, logger: Logger = None):
        """
        Constructor of the class.

        :param sacale: :py:class:`float`
            What percentage of the input array scale to provide as the output size of the processed array (1, 100)

        :param logger: :py:class:`logging.Logger`
            The logging object used to record errors and other information logged by this class.  If None, then logging
            will be sent to stderr. (Default: ``None``)
        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

    def process(self, img: numpy.ndarray) -> numpy.ndarray:
        pass
