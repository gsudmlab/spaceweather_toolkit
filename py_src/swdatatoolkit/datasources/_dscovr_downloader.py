"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import re
import gzip
import logging
import traceback
import numpy as np
import pandas as pd
import netCDF4 as nc
import urllib.request
from io import BytesIO
from logging import Logger
from bs4 import BeautifulSoup
from collections import namedtuple
from collections import OrderedDict
from pandas import DataFrame, Series
from datetime import datetime, timedelta

Range = namedtuple('Range', ['start', 'end'])


class DSCOVRDownloader:
    """
        This class downloads 1 minute averaged data from the Deep Space Climate Observatory (DSCOVR) from
        `NOAA's archive <https://www.ngdc.noaa.gov/dscovr/data/>`
    """

    def __init__(self, logger: Logger = None):
        """
        Constructor for the class.

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)

        """
        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        self._base_url = "https://www.ngdc.noaa.gov/dscovr/data/{}/{}/"
        self._date_lim = datetime(2016, 8, 1)
        self._link_filter = re.compile("^oe_f1m_dscovr_s[0-9]+_e[0-9]+_p[0-9]+_pub.nc.gz")

    def get_fc_data(self, start_time: datetime, end_time: datetime) -> DataFrame:
        """
        Method that downloads the faraday cup data that provide real-time measurement of solar wind proton density,
        speed, velocity, temperature, etc... This is downloaded the data that falls within the input range and is
        returned as a DataFrame.

        :param start_time: :py:class:`datetime.datetime` The start time of the range
        :param end_time: :py:class:`datetime.datetime` The end time of the range
        :return: :py:class:`pandas.DataFrame`
        """
        self.__validate_parameters(start_time, end_time)
        try:
            date_partitions = OrderedDict(((start_time + timedelta(_)).strftime(r"%Y-%m"), None) for _ in
                                          range((end_time - start_time).days + 1))
            # For when diff of start and end is less than a day and spans the transition from one day to the next
            date_partitions[end_time.strftime(r"%Y-%m-%d")] = None
            date_partitions = date_partitions.keys()

            input_range = Range(start=start_time, end=end_time)
            df_list = []
            for year_mo_str in date_partitions:
                year = year_mo_str[0:4]
                month = year_mo_str[5:7]
                month_url = self._base_url.format(year, month)

                files_list = []
                with urllib.request.urlopen(month_url, timeout=20) as resp:
                    soup = BeautifulSoup(resp.read(), features="html.parser")
                    for a in soup.find_all('a'):
                        if self._link_filter.match(a.text):
                            files_list.append(a.text)

                filtered_files = []
                for f in files_list:
                    start_idx = f.find('_s')
                    year_int = int(year)
                    month_int = int(month)
                    day = int(f[start_idx + 8:start_idx + 10])
                    f_start = datetime(year_int, month_int, day)
                    f_end = f_start + timedelta(days=1)
                    file_range = Range(start=f_start, end=f_end)
                    latest_start = max(input_range.start, file_range.start)
                    earliest_end = min(input_range.end, file_range.end)
                    if latest_start < earliest_end:
                        filtered_files.append(f)

                for f in filtered_files:
                    df = self.__download_file(year, month, f)
                    if df is not None and not df.empty:
                        df_list.append(df)

            if len(df_list):
                merged_df = pd.concat(df_list)
                mask = (merged_df.index > start_time) & (merged_df.index <= end_time)
                filtered_df = merged_df.loc[mask]
                filtered_df = filtered_df[~filtered_df.index.duplicated(keep='first')]
                return filtered_df
            else:
                return None
        except Exception as e:
            self._logger.error("DSCOVRDownloader.get_data failed with error: %s", str(e))
            self._logger.debug("DSCOVRDownloader.get_data failed traceback: %s", traceback.format_exc())

    def __validate_parameters(self, start_time: datetime, end_time: datetime) -> bool:

        if start_time < self._date_lim:
            raise ValueError("start_time cannot be before 2016/08/01")

        if end_time < start_time:
            raise ValueError("start_time cannot be after end_time")

        return True

    def __download_file(self, year: str, month: str, file_name: str) -> DataFrame:

        file_url = self._base_url.format(year, month) + file_name
        cols = ['sample_count', 'proton_vx_gse', 'proton_vy_gse', 'proton_vz_gse', 'proton_vx_gsm', 'proton_vy_gsm',
                'proton_vz_gsm', 'proton_speed', 'proton_density', 'proton_temperature', 'alpha_vx_gse', 'alpha_vy_gse',
                'alpha_vz_gse', 'alpha_vx_gsm', 'alpha_vy_gsm', 'alpha_vz_gsm', 'alpha_speed', 'alpha_density',
                'alpha_temperature', 'backfill_flag', 'future_packet_time_flag', 'old_packet_time_flag', 'fill_flag',
                'normal_mode_flag', 'proton_peak_unresolved_flag', 'proton_alpha_confusion_flag',
                'proton_out_of_range_high_flag', 'proton_out_of_range_low_flag', 'proton_out_of_range_unknown_flag',
                'proton_peak_unbounded_flag', 'alpha_peak_unresolved_flag', 'alpha_proton_confusion_flag',
                'alpha_out_of_range_high_flag', 'alpha_out_of_range_unknown_flag', 'alpha_peak_unbounded_flag',
                'alpha_position_low_flag', 'alpha_position_high_flag', 'alpha_peak_height_flag',
                'possible_second_proton_peak_flag', 'proton_peak_loss_flag',
                'calibration_mode_flag', 'maneuver_flag', 'reduced_proton_quality_flag', 'alpha_peak_loss_flag',
                'reduced_alpha_quality_flag', 'bad_proton_speed_flag', 'bad_proton_density_flag',
                'bad_proton_temperature_flag', 'bad_proton_vx_gse_flag', 'bad_proton_vy_gse_flag',
                'bad_proton_vz_gse_flag', 'bad_proton_vx_gsm_flag', 'bad_proton_proton_vy_gsm_flag', 'bad_vz_gsm_flag',
                'bad_alpha_speed_flag', 'bad_alpha_density_flag', 'bad_alpha_temperature_flag', 'bad_alpha_vx_gse_flag',
                'bad_alpha_vy_gse_flag', 'bad_alpha_vz_gse_flag', 'bad_alpha_vx_gsm_flag', 'bad_alpha_vy_gsm_flag',
                'bad_alpha_vz_gsm_flag', 'proton_density_convergence_failure_flag',
                'proton_temperature_convergence_failure_flag', 'proton_velocity_x_convergence_failure_flag',
                'proton_velocity_y_convergence_failure_flag', 'proton_velocity_z_convergence_failure_flag',
                'alpha_density_convergence_failure_flag', 'alpha_temperature_convergence_failure_flag',
                'alpha_velocity_x_convergence_failure_flag', 'alpha_velocity_y_convergence_failure_flag',
                'alpha_velocity_z_convergence_failure_flag', 'low_proton_density_sample_count_flag',
                'low_proton_temperature_sample_count_flag', 'low_proton_velocity_sample_count_flag',
                'low_alpha_density_sample_count_flag', 'low_alpha_temperature_sample_count_flag',
                'low_alpha_velocity_sample_count_flag', 'overall_quality']

        try:
            data_list = []
            with urllib.request.urlopen(file_url, timeout=40) as resp:
                with gzip.open(BytesIO(resp.read())) as gz:
                    with nc.Dataset('inmemory.nc', memory=gz.read()) as f:
                        idx = nc.num2date(f.variables['time'][:], f.variables['time'].units)
                        for col in cols:
                            vals = np.asarray(f.variables[col][:])
                            data_list.append(Series(vals, index=idx))
            df = DataFrame(data_list).transpose()
            df.columns = cols
            return df
        except Exception as e:
            self._logger.error("DSCOVRDownloader.__download_file failed with error: %s", str(e))
            self._logger.debug("DSCOVRDownloader.__download_file failed traceback: %s", traceback.format_exc())
