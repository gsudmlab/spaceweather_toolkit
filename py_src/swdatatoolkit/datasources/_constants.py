"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

from enum import Enum


class CACTUSReportType(Enum):
    """
    This contains all possible report types that can be downloaded from CACTUS.
    """
    CME = -2
    FLOW = -1


class ReportCat(Enum):
    """
    This contains all possible reportcats that can be downloaded from Cactus.
    """
    LEVEL_ZERO = ''
    QUICKLOOK = 'qkl'


class HARPDataType(Enum):
    """
    This contains all possible datatypes that can be downloaded for HARP data series.
    """
    NRT = 'NRT'
    DEFINITIVE = 'DEFINITIVE'


class WaveBand(Enum):
    """
    This contains the wavebands to download from JSOC for the AIA instrument
    """
    Wave_94 = 94
    Wave_131 = 131
    Wave_171 = 171
    Wave_193 = 193
    Wave_211 = 211
    Wave_304 = 304
    Wave_335 = 335
    Wave_1600 = 1600
    Wave_1700 = 1700
