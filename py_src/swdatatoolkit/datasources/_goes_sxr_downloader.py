"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import json
import logging
import traceback
import numpy as np
import pandas as pd
import netCDF4 as nc
import urllib.request
from logging import Logger
from typing import List, Dict
from collections import namedtuple
from collections import OrderedDict
from pandas import DataFrame, Series
from datetime import datetime, timedelta

Range = namedtuple('Range', ['start', 'end'])


class SXRDownloader:
    """
    This is a class to download the soft X-ray data collected from the NOAA GOES satellite fleet. It is currently
    only implemented for GOES 8 and higher, covering a range of 1995 forward.

    """

    def __init__(self, logger: Logger = None):
        """
        Constructor for the class.

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)

        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        self._data_avail_95_to_16 = {1995: {8: "ALL"},
                                     1996: {8: "ALL", 9: {'start': 4, 'end': 12}},
                                     1997: {8: "ALL", 9: "ALL"},
                                     1998: {8: "ALL", 9: {'start': 1, 'end': 7}, 10: {'start': 7, 'end': 12}},
                                     1999: {8: "ALL", 10: "ALL"},
                                     2000: {8: "ALL", 10: "ALL"},
                                     2001: {8: "ALL", 10: "ALL"},
                                     2002: {8: "ALL", 10: "ALL"},
                                     2003: {8: {'start': 1, 'end': 6}, 10: "ALL", 12: "ALL"},
                                     2004: {10: "ALL", 12: "ALL"},
                                     2005: {10: "ALL", 12: "ALL"},
                                     2006: {10: "ALL", 11: {'start': 6, 'end': 12}, 12: "ALL"},
                                     2007: {10: "ALL", 11: "ALL", 12: {'start': 1, 'end': 4}},
                                     2008: {10: "ALL", 11: {'start': 1, 'end': 2}},
                                     2009: {10: "ALL"},
                                     2010: {14: {'start': 1, 'end': 10}, 15: {'start': 11, 'end': 12}},
                                     2011: {14: {'start': 9, 'end': 12}, 15: "ALL"},
                                     2012: {14: "ALL", 15: "ALL"},
                                     2013: {14: "ALL", 15: "ALL"},
                                     2014: {14: "ALL", 15: "ALL"},
                                     2015: {13: {'start': 2, 'end': 12}, 14: {'start': 5, 'end': 6}, 15: "ALL"},
                                     2016: {13: {'start': 5, 'end': 12}, 14: {'start': 5, 'end': 12}, 15: "ALL"}
                                     }

        cols = ['start_time', 'end_time', 'satellite']
        primary_sat_list = [['1995-03-01 00:00', '2003-04-08 14:59', 8], ['2003-04-08 15:00', '2003-05-15 14:59', 10],
                            ['2003-05-15 15:00', '2006-12-31 23:59', 12], ['2007-01-01 00:00', '2007-04-11 23:59', 10],
                            ['2007-04-12 00:00', '2008-02-10 16:29', 11], ['2008-02-10 16:30', '2009-11-30 23:59', 10],
                            ['2009-12-01 00:00', '2010-10-27 23:59', 14], ['2010-10-28 00:00', '2012-10-23 15:59', 15],
                            ['2012-10-23 16:00', '2012-11-19 16:30', 14], ['2012-11-19 16:31', '2015-05-21 17:59', 15],
                            ['2015-05-21 18:00', '2015-06-09 16:24', 14], ['2015-06-09 16:25', '2016-05-03 12:59', 15],
                            ['2016-05-03 13:00', '2016-05-12 17:26', 13], ['2016-05-12 17:30', '2016-06-09 17:29', 14],
                            ['2016-06-09 17:30', '2017-02-06 23:59', 15],
                            ['2017-02-07 00:00', datetime.now().strftime('%Y-%m-%d %H:%M'), 16]]
        primary_sat_df = DataFrame(primary_sat_list, columns=cols)
        primary_sat_df['start_time'] = pd.to_datetime(primary_sat_df['start_time'])
        primary_sat_df['end_time'] = pd.to_datetime(primary_sat_df['end_time'])
        self._primary_sat_df = primary_sat_df

        self._address_95_16 = "https://www.ncei.noaa.gov/data/goes-space-environment-monitor/access/science/xrs/goes{:02d}/xrsf-l2-avg1m_science/{:04d}/{:02d}/sci_xrsf-l2-avg1m_g{:02d}_d{}_v2-2-0.nc"
        self._address_goes_r = "https://data.ngdc.noaa.gov/platforms/solar-space-observing-satellites/goes/goes{:02d}/l2/data/xrsf-l2-avg1m_science/{:04d}/{:02d}/sci_xrsf-l2-avg1m_g{:02d}_d{}_v2-2-0.nc"
        self._address_nrt = "https://services.swpc.noaa.gov/json/goes/primary/xrays-7-day.json"
        self._address_1_day_nrt = "https://services.swpc.noaa.gov/json/goes/primary/xrays-1-day.json"

        self._date_lim = datetime(1995, 3, 1)

    def _validate_parameters(self, start_time: datetime, end_time: datetime) -> bool:

        if start_time < self._date_lim:
            raise ValueError("start_time cannot be before 1995/03/01")

        if end_time < start_time:
            raise ValueError("start_time cannot be after end_time")

        return True

    def get_one_minute_avg_data(self, start_time: datetime, end_time: datetime) -> DataFrame:
        """
        Retrieves the soft X-Ray irradiance measurements averaged to a one-minute cadence of two wavelength channels,
        `xrsa` short wavelength channel irradiance (0.05 - 0.4 nm), and `xrsb` long wavelength channel irradiance
        (0.1-0.8 nm) from GOES X-ray Sensor. The satellite that is used for data retrieval is based on the primary
        GOES satellite at the specific date that the query time covers.  This is calculated for each day in the query
        period, and the resultant set of data is concatenated together for the returned DataFrame.

        Data is limited to later than March 1, 1995, any query prior to this will result in an exception being raised.

        :param start_time:  :py:class:`datetime.datetime`
            The start of the period to query for data.
        :param end_time: :py:class:`datetime.datetime`
            The end of the period to query for data.

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """
        self._validate_parameters(start_time, end_time)
        today = datetime.now()
        try:
            date_partitions = OrderedDict(((start_time + timedelta(_)).strftime(r"%Y-%m-%d"), None) for _ in
                                          range((end_time - start_time).days + 1))
            # For when diff of start and end is less than a day and spans the transition from one day to the next
            date_partitions[end_time.strftime(r"%Y-%m-%d")] = None
            date_partitions = date_partitions.keys()

            date_format = "%Y-%m-%d"

            df_list = []
            for year_mo_day_str in date_partitions:
                cur_day = datetime.strptime(year_mo_day_str, date_format)
                file_name = "{:04d}{:02d}{:02d}".format(cur_day.year, cur_day.month, cur_day.day)

                cond = ((self._primary_sat_df['start_time'] <= cur_day) & (
                        cur_day <= self._primary_sat_df['end_time']))
                sat_num = self._primary_sat_df.loc[cond, 'satellite'].values[0]

                if (today - cur_day).days < 7:
                    if (today - cur_day).days < 1:
                        address = self._address_1_day_nrt
                    else:
                        address = self._address_nrt
                    data = self.__get_json_data_from_url(address)
                    if data is not None:
                        df = self.__read_nrt_json(data)
                        if df is not None and not df.empty:
                            df_list.append(df)
                    # If we have gotten to data within the last 7 days, then we don't need to continue to process
                    break
                else:
                    if sat_num >= 16:
                        address = self._address_goes_r.format(sat_num, cur_day.year, cur_day.month, sat_num, file_name)
                        data = self.__get_data_from_url(address)
                        if data is not None:
                            with nc.Dataset('inmemory.nc', memory=data) as f:
                                df = self.__read_xrsa_xrsb_file(f)
                                if df is not None and not df.empty:
                                    df_list.append(df)
                    else:
                        address = self._address_95_16.format(sat_num, cur_day.year, cur_day.month, sat_num, file_name)
                        data = self.__get_data_from_url(address)
                        if data is not None:
                            with nc.Dataset('inmemory.nc', memory=data) as f:
                                df = self.__read_xrsa_xrsb_old(f)
                                if df is not None and not df.empty:
                                    df_list.append(df)

            if len(df_list):
                merged_df = pd.concat(df_list)
                mask = (merged_df.index > start_time) & (merged_df.index <= end_time)
                filtered_df = merged_df.loc[mask]
                filtered_df = filtered_df[~filtered_df.index.duplicated(keep='first')]
                return filtered_df
            else:
                return None
        except Exception as e:
            self._logger.error("SXRDownloader.get_one_minute_avg_data failed with error: %s", str(e))
            self._logger.debug("SXRDownloader.get_one_minute_avg_data failed traceback: %s", traceback.format_exc())

    def __get_data_from_url(self, url: str):
        try:
            with urllib.request.urlopen(url, timeout=20) as resp:
                if resp.status == 200:
                    file_data = resp.read()
                    return file_data
                else:
                    self._logger.info("SXRDownloader.__get_data_from_url did not get 200 response: %s", resp.status)
        except Exception as e:
            self._logger.error("SXRDownloader.__get_data_from_url failed with error: %s", str(e))
            self._logger.info("SXRDownloader.__get_data_from_url address that failed: %s", url)
            self._logger.debug("SXRDownloader.__get_data_from_url failed traceback: %s",
                               traceback.format_exc())

    def __get_json_data_from_url(self, url: str):
        try:
            with urllib.request.urlopen(url, timeout=20) as resp:
                if resp.status == 200:
                    file_data = resp.read()
                    result_json = json.loads(file_data)
                    return result_json
                else:
                    self._logger.info("SXRDownloader.__get_json_data_from_url did not get 200 response: %s",
                                      resp.status)
        except Exception as e:
            self._logger.error("SXRDownloader.__get_json_data_from_url failed with error: %s", str(e))
            self._logger.debug("SXRDownloader.__get_json_data_from_url failed traceback: %s",
                               traceback.format_exc())

    @staticmethod
    def __read_nrt_json(file: List[Dict]) -> DataFrame:
        """
        Method that is used to parse the NRT `xrsa` short wavelength channel irradiance (0.05 - 0.4 nm), and
        `xrsb` long wavelength channel irradiance (0.1-0.8 nm) from GOES X-ray Sensor data from NOAA's Space Weather
        Prediction Center.  These are a different format from the files that come from either the GOES-R or older GOES
        archive files.

        The `A_AVG` and `B_AVG` fields in the returned DataFrame have a fill value of -9999.0 for missing data.
        The `A_QUAL_FLAG` and `B_QUAL_FLAG` fields use 0 to indicate good data, 2 to indicate that saturation of the
        detector has occurred.
        The `A_NUM_PTS` and `B_NUM_PTS` indicate the number of measurements in the averaged value returned, but is set
        to 255 for this data to indicate it is not calculated properly because it is not feature in the input data.

        :param file: :py:class:`typing.List`
            A list of dictionary objects parsed from JSON formatted data. The objects each have a `time_tag` and
            `energy` element used to identify which timestep and energy channel the `flux` belongs

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """
        date_format = "%Y-%m-%dT%H:%M:%SZ"
        xrsa_list = []
        xrsb_list = []
        for line in file:
            time_stamp = datetime.strptime(line['time_tag'], date_format)
            flux = float(line['flux'])
            qual_flag = 0 if not line["electron_contaminaton"] else 2
            if line['energy'] == "0.05-0.4nm":
                xrsa_list.append([time_stamp, flux, qual_flag])
            else:
                xrsb_list.append([time_stamp, flux, qual_flag])
        cols1 = ['time', 'A_AVG', 'A_QUAL_FLAG']
        df1 = DataFrame(xrsa_list, columns=cols1)
        df1 = df1.set_index('time')

        cols2 = ['time', 'B_AVG', 'B_QUAL_FLAG']
        df2 = DataFrame(xrsb_list, columns=cols2)
        df2 = df2.set_index('time')

        df3 = pd.concat([df1, df2], axis=1)
        df3['A_NUM_PTS'] = 255
        df3['B_NUM_PTS'] = 255
        return df3[['A_AVG', 'B_AVG', 'A_QUAL_FLAG', 'B_QUAL_FLAG', 'A_NUM_PTS', 'B_NUM_PTS']]

    @staticmethod
    def __read_xrsa_xrsb_file(file: nc.Dataset) -> DataFrame:
        """
         Method that reads files with `xrsa` short wavelength channel irradiance (0.05 - 0.4 nm), and
        `xrsb` long wavelength channel irradiance (0.1-0.8 nm) from GOES X-ray Sensor. These are from the
        data of GOES 16 and on, ranging from 2017 onward.

        The `A_AVG` and `B_AVG` fields in the returned DataFrame have a fill value of -9999.0 for missing data.
        The `A_QUAL_FLAG` and `B_QUAL_FLAG` fields use 0 to indicate good data, 1 to indicate eclipsed data, 2 to indicate
        that saturation of the detector has occurred.
        The `A_NUM_PTS` and `B_NUM_PTS` indicate the number of measurements in the averaged value returned, and uses 255
        as a fill value when not calculated properly. This may or may not be a reliable feature.


        :param file: :py:class:`netCDF4.Dataset`
            The opened NetCDF file to parse the `xrsa` and `xrsb` features from.

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """
        data_list = []
        idx = nc.num2date(file.variables['time'][:], file.variables['time'].units)
        cols = ['xrsa_flux', 'xrsb_flux', 'xrsa_flag', 'xrsb_flag', 'xrsa_num', 'xrsb_num']

        for col in cols:
            vals = np.asarray(file.variables[col][:])
            data_list.append(Series(vals, index=idx))

        df = DataFrame(data_list).transpose()
        cols = ['A_AVG', 'B_AVG', 'A_QUAL_FLAG', 'B_QUAL_FLAG', 'A_NUM_PTS', 'B_NUM_PTS']
        df.columns = cols
        return df

    @staticmethod
    def __read_xrsa_xrsb_old(file: nc.Dataset) -> DataFrame:
        """
        Method that reads older files with `xrsa` short wavelength channel irradiance (0.05 - 0.4 nm), and
        `xrsb` long wavelength channel irradiance (0.1-0.8 nm) from GOES X-ray Sensor. These are from the reprocessed
        data of GOES 8 through 15, ranging from 1995 through 2016.

        The `xrsa_flux` and `xrsb_flux` fields in the returned DataFrame have a fill value of -9999.0 for missing data.
        The `xrsa_flags` and `xrsb_flags` fields use 0 to indicate good data, 1 to indicate eclipsed data, 2 to indicate
        that saturation of the detector has occurred.
        The `xrsa_num` and `xrsb_num` indicate the number of measurements in the averaged value returned, and uses 255
        as a fill value when not calculated properly. This may or may not be a reliable feature.


        :param file: :py:class:`netCDF4.Dataset`
            The opened NetCDF file to parse the `xrsa` and `xrsb` features from.

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """
        data_list = []
        idx = nc.num2date(file.variables['time'][:], file.variables['time'].units)
        if 'xrsa_flags' in file.variables:
            cols = ['xrsa_flux', 'xrsb_flux', 'xrsa_flags', 'xrsb_flags', 'xrsa_num', 'xrsb_num']
        else:
            cols = ['xrsa_flux', 'xrsb_flux', 'xrsa_flag', 'xrsb_flag', 'xrsa_num', 'xrsb_num']
        for col in cols:
            vals = np.asarray(file.variables[col][:])
            data_list.append(Series(vals, index=idx))

        df = DataFrame(data_list).transpose()
        cols = ['A_AVG', 'B_AVG', 'A_QUAL_FLAG', 'B_QUAL_FLAG', 'A_NUM_PTS', 'B_NUM_PTS']
        df.columns = cols
        return df
