"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import os
import re
import drms
import time
import glob
import socket
import tarfile
import datetime
import requests
import tempfile
import traceback
import urllib.request
from datetime import datetime

import logging
from logging import Logger
from typing import List, Dict, Tuple

from . import HARPDataType


class HARPDownloader:
    """
    This is a class to download the HARP Data Series, see `Spaceweather HMI Active Region Patch (SHARP) <http://jsoc.stanford.edu/doc/data/hmi/sharp/sharp.htm#data_segments>`
    documentation page for information on the data series.
    """

    def __init__(self, drms_client_email: str, data_directory: str = None, datatype: HARPDataType = HARPDataType.NRT,
                 filetypes: List[str] = None, logger: Logger = None):
        """
        Constructor for the class.

        :param drms_client_email: :py:class:`str` Registered Email Address on JSOC used to email when the request is
                                                ready to download.

        :param data_directory: :py:class:`str` Download directory to store the data when downloaded. Note: If None then
                                                a temporary directory is constructed which will be deleted upon process
                                                ending. So, if using the temp directory the data must be moved from this
                                                location prior the process ends.
                                                (Default: ``None``)

        :param datatype: :py:class:`HARPDataType` The datatype to be processed. e.g NRT(for near realtime),
                                                DEFINITIVE(for definitive).
                                                (Default: ``NRT``)

        :param filetypes: :py:class:`typing.List` List of filetypes to process. Options include 'Br', 'Br_err', 'Bt', 'Bt_err', 'Bp', 'Bp_err', 'bitmap', 'conf_disambig',
                                    'magnetogram', 'Dopplergram', 'continuum'.  If none, then ALL are selected.
                                    (Default: ``None``)

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)

        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        if datatype is HARPDataType.DEFINITIVE:
            self._qfilter = 'hmi.sharp_cea_720s'
        else:
            self._qfilter = 'hmi.sharp_cea_720s_nrt'

        if data_directory is None:
            self._tmp_file = tempfile.TemporaryDirectory()
            self._data_directory = self._tmp_file.name
        else:
            self._data_directory = self.__validate_data_directory(data_directory)

        if filetypes is None:
            filetypes = ['all']
        self.__validate_filetypes(filetypes)

        if filetypes[0] == 'all':
            self._file_type_list = ['Br', 'Br_err', 'Bt', 'Bt_err', 'Bp', 'Bp_err', 'bitmap', 'conf_disambig',
                                    'magnetogram', 'Dopplergram', 'continuum']
        else:
            self._file_type_list = filetypes

        self._file_type_filter = '{' + ','.join(filetypes) + '}'

        self._drms_client_name = drms_client_email
        socket.setdefaulttimeout(60)

    def __validate_filetypes(self, filetypes):
        c = drms.Client(verbose=False)
        allowed_filetypes = c.info(self._qfilter).segments.index.values.tolist()
        for filetype in filetypes:
            if filetype not in allowed_filetypes:
                raise ValueError(
                    f"'{filetype}' is not Valid in filetypes. Should be any of {','.join(allowed_filetypes)}")

    @staticmethod
    def __validate_data_directory(data_directory):
        if not os.path.exists(data_directory):
            raise ValueError(f"Directory '{data_directory}' does not exist")
        return data_directory

    def __remove_downloaded_from_online(self, harp_records: dict) -> Dict:
        """
        Cleans the list of online records to remove those that have already been downloaded. It is expected that each
        HARP number in the dictionary has a list of online records to download into a directory that is named after
        the HARP number being processed.

        :param harp_records: Dictionary of HARP numbers to process with each entry being a list of file times to process
        :return: Cleaned dictionary of online records for each harp number
        """

        answer = {}
        for sNum, recs in harp_records.items():
            tracker = {}
            save_dir = os.path.join(self._data_directory, str(sNum))

            if not os.path.exists(save_dir):
                answer[sNum] = recs
            else:
                self.__check_for_tar(save_dir)
                for filetype in self._file_type_list:

                    files = []
                    files.extend(glob.glob(save_dir + f"/*{filetype}.fits"))

                    seen = []
                    for file in files:
                        dots = list(self.__find_all(file, '.'))
                        if len(dots) > 2:
                            yr_pos = dots[2] + 1
                            yr = file[yr_pos:yr_pos + 4]
                            mo_pos = yr_pos + 4
                            mo = file[mo_pos:mo_pos + 2]
                            dy_pos = mo_pos + 2
                            dy = file[dy_pos:dy_pos + 2]
                            hr_pos = dy_pos + 3
                            hr = file[hr_pos:hr_pos + 2]
                            mi_pos = hr_pos + 2
                            mi = file[mi_pos:mi_pos + 2]
                            sec_pos = mi_pos + 2
                            sec = file[sec_pos:sec_pos + 2]
                            rec_time = "{0}.{1}.{2}_{3}:{4}:{5}_TAI".format(yr, mo, dy, hr, mi, sec)
                            if rec_time in recs:
                                seen.append(rec_time)

                    tracker[filetype] = list(set(seen))

                all_filetypes = []
                for record in tracker:
                    all_filetypes.append(tracker[record])
                all_filetypes = [item for sublist in all_filetypes for item in sublist]

                for rec in recs.copy():
                    if all_filetypes.count(rec) == len(self._file_type_list):
                        recs.remove(rec)
                if len(recs) > 0:
                    answer[sNum] = recs

        return answer

    @staticmethod
    def __find_all(a_str: str, sub: str):
        start = 0
        while True:
            start = a_str.find(sub, start)
            if start == -1: return
            yield start
            start += len(sub)

    def __check_for_tar(self, save_dir: str) -> bool:
        """
        Method checks for and unzips tar files within a specified directory.  It will also remove some of the additional
        files that are sometimes included in the tar file as well as partial downloads and the tar file itself.

        :param save_dir: str
            The directory to search for tar files to unzip

        :return: True if a tar file was found to unzip, false otherwise.
        """

        found_files = False
        files = []
        files.extend(glob.glob(save_dir + "/*.tar"))

        for fname in files:
            if fname.endswith("tar"):
                try:
                    tar = tarfile.open(fname, "r:")
                    tar.extractall(save_dir)
                    tar.close()
                    found_files = True
                except Exception as e:
                    self._logger.error('Check for Tar error: %s', str(e))
                    self._logger.debug('Traceback: %s', traceback.format_exc())
                os.remove(fname)

        types = ('*.txt', '*.html', '*.json', '*.drmsrun', '*.qsub', '*.tar.*')
        files = []
        for ftype in types:
            files.extend(glob.glob(save_dir + "/" + ftype))

        for f in files:
            os.remove(f)

        return found_files

    def __download_online_records(self, harp_records: dict) -> Tuple[List[int], str]:
        """
        Downloads the records for each HARP in the dictionary that is passed. It is expected that each HARP number
        in the dictionary has a list of online records to download into a directory that is named after the HARP
        number being processed.

        :param harp_records: Dictionary of HARP numbers to process with each entry being a list of file times to process
        :return: A tuple where the first element is the list of HARP numbers that were successfully downloaded and the
                second element is the storage directory where the HARP subdirectories are located.
        """
        processed_harp_nums = []
        part_size = 100
        for harp_number, recs in harp_records.items():
            self._logger.info("SharpNum {0}, Records {1}".format(harp_number, len(recs)))

            save_dir = os.path.join(self._data_directory, str(harp_number))
            has_complete = False
            attempt_count = 0
            if not os.path.exists(save_dir):
                os.makedirs(save_dir)

            while attempt_count < 5 and not has_complete:
                try:
                    self._logger.info('Total records found: %s - HARP: %s', len(recs), harp_number)
                    if len(recs) > 0:
                        c = drms.Client()
                        num_partitions = len(recs) // part_size

                        for i in range(num_partitions + 1):
                            ts_tmp = ''
                            if i < num_partitions:
                                for j in range(0, part_size):
                                    ts_tmp += '{0}, '.format(recs[j + i * part_size])
                            else:
                                j = i * part_size
                                while j < len(recs):
                                    ts_tmp += '{0}, '.format(recs[j])
                                    j = j + 1

                            q = f"{self._qfilter}[{harp_number}][{ts_tmp}]"

                            k = c.query(q, key='HARPNUM, T_REC')
                            self._logger.debug("Found %s records for query on HARP:%s", len(k), harp_number)
                            max_req = 25000

                            self._logger.debug("Starting Download Harp:%s, Partition:%s", harp_number, i)

                            r = c.export(
                                q + self._file_type_filter,
                                method='url-tar', protocol='fits', n=max_req, email=self._drms_client_name)

                            self._logger.debug('Starting download...')
                            time.sleep(10)

                            try_cnt = 0
                            done = False
                            while try_cnt < 10 and not done:
                                while (not r.has_finished()) and (not r.has_failed()):
                                    r.wait(sleep=30, timeout=120)

                                tarurl = r.urls['url'][0]
                                filename = r.urls['filename'][0]
                                save_loc = os.path.join(save_dir, filename)
                                try:
                                    # make request and set timeout for no data to 120 seconds and enable streaming
                                    with urllib.request.urlopen(tarurl, timeout=120) as resp:
                                        # Open output file and make sure we write in binary mode
                                        with open(save_loc + '.part', 'wb') as fh:
                                            # walk through the request response in chunks of 1MiB
                                            while True:
                                                chunk = resp.read(1024 * 1024)
                                                if not chunk:
                                                    break
                                                # Write the chunk to the file
                                                fh.write(chunk)

                                    # Rename the file
                                    os.rename(save_loc + '.part', save_loc)

                                    done = True
                                    self._logger.info("Data Chunk Downloaded - HARP:%s, Partition:%s", harp_number, i)
                                    if self.__check_for_tar(save_dir):
                                        processed_harp_nums.append(harp_number)
                                except Exception as e:
                                    self._logger.error('Attempt:%s. - Download Chunk Error: %s - HARP:%s',
                                                       attempt_count, str(e), harp_number)
                                    self._logger.debug('Download Chunk Error Traceback: %s - HARP:%s',
                                                       traceback.format_exc(), harp_number)
                                    time.sleep(10)
                                    try_cnt += 1
                        has_complete = True
                    else:
                        self._logger.info("No records found - HARP:%s", harp_number)
                        None
                    has_complete = True
                except Exception as e:
                    self._logger.error('Attempt:%s - Download Chunk Error: %s - HARP:%s', attempt_count, str(e),
                                       harp_number)
                    self._logger.debug('Download Chunk Error Traceback: %s - HARP:%s', traceback.format_exc(),
                                       harp_number)
                    time.sleep(10)
                    attempt_count += 1
        response = dict()
        response['data_directory'] = self._data_directory
        response['HarpNum'] = list(set(processed_harp_nums))
        return response

    def __get_list_of_online_by_time_range(self, start_time: datetime, end_time: datetime) -> Dict:
        """
        Gets a json object that contains a dictionary of all the online records for the HARP dataset. The
        dictionary contains an entry for each HARP that is found in the range, and each entry is a list of files
        available for that harp number in the given date range.

        :param start_time: :py:class:`datetime.datetime` The start time of the range
        :param end_time: :py:class:`datetime.datetime` The end time of the range
        :return: Dictionary with a list of online file times to download from JSOC for each HARP number found in range.
        """

        start_time_format = start_time.strftime("%Y.%m.%d_%H:%M:%S")
        end_time_format = end_time.strftime("%Y.%m.%d_%H:%M:%S")

        time_range = start_time_format + "-" + end_time_format

        ds_param = f"{self._qfilter}[][{time_range}]"

        return self.__get_list_of_online_with_filter(ds_param)

    def __get_list_of_online_by_harp_number(self, harp_number: int) -> Dict:
        """
        Gets a json object that contains a dictionary of all the online records for the HARP dataset. The
        dictionary contains an entry for the specified HARP that is found, and is a list of files
        available for that harp number.

        :param harp_number: :py:class:`int` The Harp Num to downloading data for
        :return: Dictionary with a list of online file times to download from JSOC for the HARP number
        """

        ds_param = f"{self._qfilter}[{harp_number}][]"

        return self.__get_list_of_online_with_filter(ds_param)

    def __get_list_of_online_by_harp_number_and_range(self, harp_number: int, start_time: datetime,
                                                      end_time: datetime) -> Dict:
        """
        Gets a json object that contains a dictionary of all the online records for the HARP dataset. The
        dictionary contains an entry for the specified HARP that is found, and is a list of files
        available for that harp number in the given date range.

        :param harp_number: :py:class:`int` The Harp Num to downloading data for
        :param start_time: :py:class:`datetime.datetime` The start time of the range
        :param end_time: :py:class:`datetime.datetime` The end time of the range
        :return: Dictionary with a list of online file times to download from JSOC for the HARP number
        """

        start_time_format = start_time.strftime("%Y.%m.%d_%H:%M:%S")
        end_time_format = end_time.strftime("%Y.%m.%d_%H:%M:%S")

        time_range = start_time_format + "-" + end_time_format

        ds_param = f"{self._qfilter}[{harp_number}][{time_range}]"

        return self.__get_list_of_online_with_filter(ds_param)

    def __get_list_of_online_with_filter(self, ds_filter: str) -> Dict:
        """
        This function accesses the `jsoc_info <http://jsoc.stanford.edu/doxygen_html/group__jsoc__info.html>` API to get
        information about the dataset, specifically which files are actually accessible and ready to request. This was
        introduced because some NRT data was found to be inaccessible and caused the DRMS request to fail when making a
        request that included the offline file time steps. The function returns a json object that contains a dictionary
        of all the online records for dataset. The dictionary contains an entry for each HARP that is found in the range
        specified by the input dataset filter, and each entry is a list of files available for that harp number in the
        given dataset range.

        :param ds_filter: :py:class:`str` The data set filter string that is passed to the jsoc_info function.
        :return: Dictionary with a list of online file times to download from JSOC for each HARP number found in range.
                 In cases that an error occurred and retying did not resolve the issue, None is returned.
        """

        url = 'http://jsoc.stanford.edu/cgi-bin/ajax/jsoc_info'

        answer = {}

        t_rec_pattern = f"{self._qfilter}\\[(.*?)\\]\\[(.*?)\\]"
        params = dict(
            ds=ds_filter,
            op='rs_list',
            key='*online*,*spec*',
            f=1
        )

        try_count = 0
        done = False
        while try_count < 5 and not done:
            try:
                resp = requests.get(url=url, params=params, timeout=60)
                if resp.status_code == 200:
                    data = resp.json()
                    for key, values in data.items():
                        if key == "recset":
                            for val in values:
                                if val["online"] == "Y":
                                    t_record = re.search(t_rec_pattern, val["spec"])
                                    if t_record.group(1) in answer:
                                        answer[t_record.group(1)].append(t_record.group(2))
                                    else:
                                        answer[t_record.group(1)] = [t_record.group(2)]

                    self._logger.info("Online Records JSON Downloaded - for filter:%s", ds_filter)
                    done = True
                else:
                    self._logger.critical("Online Records HTTP request Failed: %s - for filter:%s", resp.status_code,
                                          ds_filter)
                    done = False
                    try_count += 1

            except Exception as e:
                self._logger.critical("Online Records JSON Download Failed. Error:%s - for filter:%s", str(e),
                                      ds_filter)
                try_count += 1

        if done:
            return answer
        else:
            return None

    def download_records_for_range(self, start_time: datetime, end_time: datetime) -> Tuple[List[int], str]:
        """
        Downloads harp data between the start and end time provided. If the data already exists on disk, then nothing
        is done.

        :param start_time: :py:class:`datetime.datetime` The start time to begin downloading data from

        :param end_time: :py:class:`datetime.datetime` The end time to end downloading data from

        :return: :py:class:`typing.Tuple` A tuple where the first element is the list of HARP numbers that were
                successfully downloaded and the second element is the storage directory where the HARP subdirectories
                are located.
        """
        online_files = self.__get_list_of_online_by_time_range(start_time, end_time)
        online_files = self.__remove_downloaded_from_online(online_files)
        return self.__download_online_records(online_files)

    def download_records_for_harp_number(self, harp_number: int) -> Tuple[List[int], str]:
        """
        Downloads harp data for sharp Number provided. If the data already exists on disk, then nothing
        is done.

        :param harp_number: The Harp Num to downloading data for

        :return: :py:class:`typing.Tuple` A tuple where the first element is the list of HARP numbers that were
                successfully downloaded and the second element is the storage directory where the HARP subdirectories
                are located.
        """
        online_files = self.__get_list_of_online_by_harp_number(harp_number)
        online_files = self.__remove_downloaded_from_online(online_files)
        return self.__download_online_records(online_files)

    def download_records_for_harp_number_and_range(self, harp_number: int, start_time: datetime, end_time: datetime) -> \
            Tuple[List[int], str]:
        """
        Downloads harp data for sharp Number provided. If the data already exists on disk, then nothing
        is done.

        :param harp_number: :py:class:`int`
            The Harp Num to downloading data for

        :param start_time: :py:class:`datetime.datetime`
            The start time to begin downloading data from

        :param end_time: :py:class:`datetime.datetime`
            The end time to end downloading data from

        :return: :py:class:`typing.Tuple`
            A tuple where the first element is the list of HARP numbers that were
            successfully downloaded and the second element is the storage directory where the HARP subdirectories
            are located.
        """
        online_files = self.__get_list_of_online_by_harp_number_and_range(harp_number, start_time, end_time)
        online_files = self.__remove_downloaded_from_online(online_files)
        return self.__download_online_records(online_files)

    def get_harp_process_date(self, harp_number: int) -> datetime:
        """
        Gets the most recent processing date for a HARP from JSOC. These could be different from what's on disk.

        :param harp_number:  :py:class:`int`
            The harp number to retrieve the data for

        :return: :py:class:`datetime.datetime`
            The date of either the last SHARP processing or the B input data processing, whichever is later (likely
            the SHARP processing as it would be done after the B data was processed).
        """

        ds_param = f"{self._qfilter}[{harp_number}]"
        try:
            client = drms.Client()
            query = client.query(ds_param, key=['DATE', 'DATE_B'])
            if not query is None:
                sharp_date = query[['DATE']].sort_values(by=['DATE'], ascending=False).iloc[0].DATE
                sharp_date = datetime.strptime(sharp_date, '%Y-%m-%dT%H:%M:%SZ')
                b_data_date = query[['DATE_B']].sort_values(by=['DATE_B'], ascending=False).iloc[0].DATE_B
                b_data_date = datetime.strptime(b_data_date, '%Y-%m-%dT%H:%M:%SZ')
                if sharp_date > b_data_date:
                    return sharp_date
                else:
                    return b_data_date

        except Exception as e:
            self._logger.error('HARPDownloader.get_harp_process_date for HARP %s failed with: %s', harp_number, str(e))
            self._logger.debug('HARPDownloader.get_harp_process_date Error Traceback: %s ', traceback.format_exc())
