"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
import urllib
import logging
import traceback
import numpy as np
import pandas as pd
import urllib.request
from logging import Logger
from typing import List, Dict
from collections import namedtuple
from collections import OrderedDict
from pandas import DataFrame
from datetime import datetime, timedelta

Range = namedtuple('Range', ['start', 'end'])


class ProtonDownloader:
    """
    This is a class to download the proton data collected from the NOAA GOES satellite fleet. It is currently
    only implemented for GOES 14 and higher, covering a range of April 1, 2010, forward.
    """

    def __init__(self, logger: Logger = None):
        """
        Constructor for the class.

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)

        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        self._gsfc_url = "https://iswa.gsfc.nasa.gov/IswaSystemWebApp/hapi/data"
        self._address_nrt = "https://services.swpc.noaa.gov/json/goes/primary/integral-protons-7-day.json"
        self._address_1_day_nrt = "https://services.swpc.noaa.gov/json/goes/primary/integral-protons-1-day.json"

        self._date_lim = datetime(2010, 4, 1)

    def _validate_parameters(self, start_time: datetime, end_time: datetime) -> bool:

        if start_time < self._date_lim:
            raise ValueError("start_time cannot be before 2010/04/01")

        if end_time < start_time:
            raise ValueError("start_time cannot be after end_time")

        return True

    def get_five_min_avg_data(self, start_time: datetime, end_time: datetime) -> DataFrame:
        """
        Retrieves GOES proton data with the `time`, `>=1 MeV`, `>=10 MeV`, `>=100 MeV`, `>=30 MeV`, `>=5 MeV`,
        `>=50 MeV`, `>=500 MeV`, and `>=60 MeV` fields from the ISWA database or from NOAA's Space Weather Prediction
        Center's 1 and 7 Day primary GOES satellite integral-protons files. This is calculated for each day in the query
        period, and the resultant set of data is concatenated together for the returned DataFrame.

        Data is limited to later than April 1, 2010, any query prior to this will result in an exception being raised.

        :param start_time:  :py:class:`datetime.datetime`
            The start of the period to query for data.
        :param end_time: :py:class:`datetime.datetime`
            The end of the period to query for data.

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """
        self._validate_parameters(start_time, end_time)
        today = datetime.now()
        try:
            date_partitions = OrderedDict(((start_time + timedelta(_)).strftime(r"%Y-%m-%d"), None) for _ in
                                          range((end_time - start_time).days + 1))
            # For when diff of start and end is less than a day and spans the transition from one day to the next
            date_partitions[end_time.strftime(r"%Y-%m-%d")] = None
            date_partitions = date_partitions.keys()

            date_format = "%Y-%m-%d"
            out_date_format = "%Y-%m-%d %H:%M:%S"

            df_list = []
            for year_mo_day_str in date_partitions:
                cur_day = datetime.strptime(year_mo_day_str, date_format)

                if (today - cur_day).days < 7:
                    if (today - cur_day).days < 1:
                        address = self._address_1_day_nrt
                    else:
                        address = self._address_nrt
                    data = self.__get_json_data_from_url(address)
                    if data is not None:
                        df = self.__read_nrt_json(data)
                        if df is not None and not df.empty:
                            df_list.append(df)
                    # If we have gotten to data within the last 7 days, then we don't need to continue to process
                    break
                else:
                    cur_start_str = cur_day.strftime(out_date_format)
                    cur_end_str = (cur_day + timedelta(days=1)).strftime(out_date_format)
                    address = self._gsfc_url.format(cur_start_str, cur_end_str)
                    contet_dict = {'id': 'goesp_part_flux_P5M',
                                   'time.min': cur_start_str,
                                   'time.max': cur_end_str,
                                   'format': 'json'
                                   }
                    data = self.__get_json_data_from_url_query(address, contet_dict)
                    if data is not None:
                        df = self.__read_iswa_json(data)
                        if df is not None and not df.empty:
                            df_list.append(df)

            if len(df_list):
                merged_df = pd.concat(df_list)
                mask = (merged_df.index > start_time) & (merged_df.index <= end_time)
                filtered_df = merged_df.loc[mask]
                filtered_df = filtered_df[~filtered_df.index.duplicated(keep='first')]
                return filtered_df
            else:
                return None
        except Exception as e:
            self._logger.error("ProtonDownloader.get_one_minute_avg_data failed with error: %s", str(e))
            self._logger.debug("ProtonDownloader.get_one_minute_avg_data failed traceback: %s", traceback.format_exc())

    def __get_data_from_url(self, url: str):
        try:
            with urllib.request.urlopen(url, timeout=30) as resp:
                if resp.status == 200:
                    file_data = resp.read()
                    return file_data
                else:
                    self._logger.info("ProtonDownloader.__get_data_from_url did not get 200 response: %s", resp.status)
        except Exception as e:
            self._logger.error("ProtonDownloader.__get_data_from_url failed with error: %s", str(e))
            self._logger.debug("ProtonDownloader.__get_data_from_url failed traceback: %s",
                               traceback.format_exc())

    def __get_json_data_from_url(self, url: str):
        try:
            with urllib.request.urlopen(url, timeout=30) as resp:
                if resp.status == 200:
                    file_data = resp.read()
                    result_json = json.loads(file_data)
                    return result_json
                else:
                    self._logger.info("ProtonDownloader.__get_json_data_from_url did not get 200 response: %s",
                                      resp.status)
        except Exception as e:
            self._logger.error("ProtonDownloader.__get_json_data_from_url failed with error: %s", str(e))
            self._logger.debug("ProtonDownloader.__get_json_data_from_url failed traceback: %s",
                               traceback.format_exc())

    def __get_json_data_from_url_query(self, url: str, qury_dict: Dict):
        try:
            url_qury = urllib.parse.urlencode(qury_dict)
            url_qury = "{}?{}".format(url, url_qury)
            with urllib.request.urlopen(url_qury, timeout=30) as resp:
                if resp.status == 200:
                    file_data = resp.read()
                    result_json = json.loads(file_data)
                    return result_json
                else:
                    self._logger.info("ProtonDownloader.__get_json_data_from_url_query did not get 200 response: %s",
                                      resp.status)
        except Exception as e:
            self._logger.error("ProtonDownloader.__get_json_data_from_url_query failed with error: %s", str(e))
            self._logger.debug("ProtonDownloader.__get_json_data_from_url_query failed traceback: %s",
                               traceback.format_exc())

    @staticmethod
    def __read_nrt_json(file_data: List[Dict]) -> DataFrame:
        """
        Method that is used to parse the NRT GOES Proton Sensor data from NOAA's Space Weather
        Prediction Center.

        The `>=1 MeV`, `>=10 MeV`, `>=100 MeV`, `>=30 MeV`, `>=5 MeV`, `>=50 MeV`, `>=500 MeV`, and `>=60 MeV`  fields
        in the returned DataFrame have a fill value of -100000.0 for missing data.

        :param file_data: :py:class:`typing.List`
            A list of dictionary objects parsed from JSON formatted data. The objects each have a `time_tag` and
            `energy` element used to identify which timestep and energy channel the `flux` belongs

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """
        date_format = "%Y-%m-%dT%H:%M:%SZ"
        p1mev_list = []
        p10mev_list = []
        p100mev_list = []
        p30mev_list = []
        p5mev_list = []
        p50mev_list = []
        p500mev_list = []
        p60mev_list = []
        for line in file_data:
            time_stamp = datetime.strptime(line['time_tag'], date_format)
            flux = float(line['flux'])
            if np.isnan(flux):
                flux = None

            if line['energy'] == ">=1 MeV":
                p1mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=10 MeV":
                p10mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=100 MeV":
                p100mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=30 MeV":
                p30mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=5 MeV":
                p5mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=50 MeV":
                p50mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=500 MeV":
                p500mev_list.append([time_stamp, flux])
            elif line['energy'] == ">=60 MeV":
                p500mev_list.append([time_stamp, flux])

        cols1 = ['time', ">=1 MeV"]
        df1 = DataFrame(p1mev_list, columns=cols1)
        df1 = df1.set_index('time')

        cols2 = ['time', ">=10 MeV"]
        df2 = DataFrame(p10mev_list, columns=cols2)
        df2 = df2.set_index('time')

        cols3 = ['time', ">=100 MeV"]
        df3 = DataFrame(p100mev_list, columns=cols3)
        df3 = df3.set_index('time')

        cols4 = ['time', ">=30 MeV"]
        df4 = DataFrame(p30mev_list, columns=cols4)
        df4 = df4.set_index('time')

        cols5 = ['time', ">=5 MeV"]
        df5 = DataFrame(p5mev_list, columns=cols5)
        df5 = df5.set_index('time')

        cols6 = ['time', ">=50 MeV"]
        df6 = DataFrame(p50mev_list, columns=cols6)
        df6 = df6.set_index('time')

        cols7 = ['time', ">=500 MeV"]
        df7 = DataFrame(p500mev_list, columns=cols7)
        df7 = df7.set_index('time')

        cols8 = ['time', ">=60 MeV"]
        df8 = DataFrame(p60mev_list, columns=cols8)
        df8 = df8.set_index('time')

        result_df = pd.concat([df1, df2, df3, df4, df5, df6, df7, df8], axis=1)
        return result_df

    @staticmethod
    def __read_iswa_json(file_data: Dict[str, List]) -> DataFrame:
        """
        Method that is used to parse the NRT GOES Proton Sensor data from NOAA's Space Weather
        Prediction Center stored at ISWA.

        The `>=1 MeV`, `>=10 MeV`, `>=100 MeV`, `>=30 MeV`, `>=5 MeV`, `>=50 MeV`, `>=500 MeV`, and `>=60 MeV`  fields
        in the returned DataFrame have a fill value of -100000.0 for missing data.

        :param file_data: :py:class:`typing.Dict`
            A dictionary with a list data object parsed from JSON formatted data. The arrays in each row represent the
            data to be returned in the DataFrame

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """

        date_format = "%Y-%m-%dT%H:%M:%SZ"

        data_list = []
        if 'data' in file_data and file_data['data'] is not None:
            json_data = file_data['data']
            for line in json_data:
                time_stamp = datetime.strptime(line[0], date_format)
                p1mev_flux = float(line[1])
                p5mev_flux = float(line[2])
                p10mev_flux = float(line[3])
                p30mev_flux = float(line[4])
                p50mev_flux = float(line[5])
                p100mev_flux = float(line[6])
                p60mev_flux = float(line[10])
                p500mev_flux = float(line[11])
                data_list.append(
                    [time_stamp, p1mev_flux, p10mev_flux, p100mev_flux, p30mev_flux, p5mev_flux, p50mev_flux,
                     p500mev_flux, p60mev_flux])

            cols = ['time', ">=1 MeV", ">=10 MeV", ">=100 MeV", ">=30 MeV", ">=5 MeV", ">=50 MeV", ">=500 MeV",
                    ">=60 MeV"]
            df = DataFrame(data_list, columns=cols)
            df = df.set_index('time')
            return df
        else:
            return None
