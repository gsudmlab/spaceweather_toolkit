"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import ssl
import json
import urllib
import logging
import traceback
import pandas as pd
import urllib.request
from logging import Logger
from typing import List, Dict
from collections import namedtuple
from collections import OrderedDict
from pandas import DataFrame
from datetime import datetime, timedelta

Range = namedtuple('Range', ['start', 'end'])
ssl._create_default_https_context = ssl._create_unverified_context


class NeutronDownloader:
    """
    This is a class to download the neutron data collected from the University of
    `Oulu neutron monitor database <https://cosmicrays.oulu.fi/>`. It is currently implemented data covering a range of
    January 1, 1997, forward.
    """

    def __init__(self, logger: Logger = None):
        """
        Constructor for the class.

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)

        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        self._url = "https://cosmicrays.oulu.fi/webform/onlinequery.cgi"

        self._date_lim = datetime(2010, 4, 1)
        self._dom_date_lim = datetime(2016, 3, 12)

    def _validate_parameters(self, start_time: datetime, end_time: datetime, monitor: str) -> bool:

        if monitor == 'OULU':
            if start_time < self._date_lim:
                raise ValueError("start_time cannot be before 2010/04/01")
        else:
            if start_time < self._dom_date_lim:
                raise ValueError("start_time cannot be before 2016/03/12 for `DOMB` and `DOMC`")

        if end_time < start_time:
            raise ValueError("start_time cannot be after end_time")

        if monitor == 'OULU' or monitor == 'DOMC' or monitor == 'DOMB':
            return True
        else:
            raise ValueError("monitor can only be one of `OULU`, `DOMC`, or `DOMB`")

    def get_one_min_avg_data(self, start_time: datetime, end_time: datetime, monitor: str = 'OULU') -> DataFrame:
        """
        Retrieves uncorrected and corrected (for pressure and efficiency ) count rates [counts/min], and barometric
        pressure [mbar] of either OULU, DOMC, or DOMB, neutron monitors. This is calculated for each day in the
        query period, and the resultant set of data is concatenated together for the returned DataFrame.

        Data is limited to later than April 1, 2010, any query prior to this will result in an exception being raised
        for the OULU sensor, and March 12, 2016, for DOMB and DOMC.

        :param start_time:  :py:class:`datetime.datetime`
            The start of the period to query for data.
        :param end_time: :py:class:`datetime.datetime`
            The end of the period to query for data.
        :param monitor: :py:class:`str`
            The neutron monitor station to query form the source database. Can be one of `OULU`, `DOMC`, or `DOMB` and
            will default to `OULU`

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame, or None if nothing was retrieved
        """
        self._validate_parameters(start_time, end_time, monitor)

        try:
            date_partitions = OrderedDict(((start_time + timedelta(_)).strftime(r"%Y-%m-%d"), None) for _ in
                                          range((end_time - start_time).days + 1))
            # For when diff of start and end is less than a day and spans the transition from one day to the next
            date_partitions[end_time.strftime(r"%Y-%m-%d")] = None
            date_partitions = date_partitions.keys()

            date_format = "%Y-%m-%d"
            out_time_format = "%H:%M"

            df_list = []
            for year_mo_day_str in date_partitions:
                cur_day = datetime.strptime(year_mo_day_str, date_format)

                cur_start_time_str = cur_day.strftime(out_time_format)
                cur_end_date = (cur_day + timedelta(days=1))
                cur_end_time_str = cur_end_date.strftime(out_time_format)

                content_dict = {'station': monitor,
                                'startday': f"{cur_day.day:02}",
                                'startmonth': f"{cur_day.month:02}",
                                'startyear': f"{cur_day.year:04}",
                                'starttime': cur_start_time_str,
                                'endday': f"{cur_end_date.day:02}",
                                'endmonth': f"{cur_end_date.month:02}",
                                'endyear': f"{cur_end_date.year:04}",
                                'endtime': cur_end_time_str,
                                'resolution': 1,
                                'outputmode': 'json'
                                }
                data = self.__get_json_data_from_url_query(self._url, content_dict)
                if data is not None:
                    df = self.__read_json(data)
                    if df is not None and not df.empty:
                        df_list.append(df)

            if len(df_list):
                merged_df = pd.concat(df_list)
                mask = (merged_df.index > start_time) & (merged_df.index <= end_time)
                filtered_df = merged_df.loc[mask]
                filtered_df = filtered_df[~filtered_df.index.duplicated(keep='first')]
                return filtered_df
            else:
                return None
        except Exception as e:
            self._logger.error("NeutronDownloader.get_one_minute_avg_data failed with error: %s", str(e))
            self._logger.debug("NeutronDownloader.get_one_minute_avg_data failed traceback: %s", traceback.format_exc())

    def __get_json_data_from_url_query(self, url: str, qury_dict: Dict):
        try:
            url_qury = urllib.parse.urlencode(qury_dict)
            url_qury = "{}?{}".format(url, url_qury)
            with urllib.request.urlopen(url_qury, timeout=20) as resp:
                if resp.status == 200:
                    file_data = resp.read()
                    result_json = json.loads(file_data)
                    return result_json
                else:
                    self._logger.info("NeutronDownloader.__get_json_data_from_url_query did not get 200 response: %s",
                                      resp.status)
        except Exception as e:
            self._logger.error("NeutronDownloader.__get_json_data_from_url_query failed with error: %s", str(e))
            self._logger.info("NeutronDownloader.__get_json_data_from_url_query failed on url: %s", url_qury)
            self._logger.debug("NeutronDownloader.__get_json_data_from_url_query failed traceback: %s",
                               traceback.format_exc())

    @staticmethod
    def __read_json(file_data: Dict[str, List]) -> DataFrame:
        """
        Method that is used to parse the Neutron Monitor database results.

        The `uncorr`, `corr`, `pressure`,  fields in a returned DataFrame, wth the `time` in the index

        :param file_data: :py:class:`typing.Dict`
            A dictionary with a list data object parsed from JSON formatted data. The arrays in each row represent the
            data to be parsed for return in the DataFrame

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the `time` parsed into datetime objects and used as the
            index for the rows of the DataFrame
        """

        date_format = "%Y-%m-%dT%H:%M:%SZ"

        data_list = []
        if 'data' in file_data and file_data['data'] is not None:
            json_data = file_data['data']
            for line in json_data:
                time_stamp = datetime.strptime(line['time:'], date_format)
                uncorr = line['uncorr:']
                corr = line['corr:']
                pressure = line['pressure:']
                data_list.append([time_stamp, uncorr, corr, pressure])

            cols = ['time', 'uncorr', 'corr', 'pressure']
            df = DataFrame(data_list, columns=cols)
            df = df.set_index('time')
            return df
        else:
            return None
