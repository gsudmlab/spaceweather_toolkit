"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import drms
import glob
import time
import socket
import logging
import tarfile
import datetime
import tempfile
import traceback
import urllib.request
from typing import List
from datetime import timedelta
from logging import Logger

from sunpy.net.jsoc import JSOCResponse

from . import WaveBand

from sunpy.net import jsoc
import astropy.units as u
from sunpy.net import attrs as a


class AIADownloader:
    """
    This is a class to download level 1 AIA data from the JSOC at Stanford.
    """

    def __init__(self, drms_client_email: str, data_directory: str = None, logger: Logger = None):
        """
        Constructor for the class.

        :param drms_client_email: :py:class:`str` Registered Email Address on JSOC used to email when the request is
                                                ready to download.

        :param data_directory: :py:class:`str` Download directory to store the data when downloaded. Note: If None then
                                                a temporary directory is constructed which will be deleted upon process
                                                ending. So, if using the temp directory the data must be moved from this
                                                location prior the process ends.
                                                (Default: ``None``)

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)
        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        if data_directory is None:
            self._tmp_file = tempfile.TemporaryDirectory()
            self._data_directory = self._tmp_file.name
        else:
            self._data_directory = self.__validate_data_directory(data_directory)

        self._drms_client_name = drms_client_email
        socket.setdefaulttimeout(60)

    @staticmethod
    def __validate_data_directory(data_directory):
        if not os.path.exists(data_directory):
            raise ValueError(f"Directory '{data_directory}' does not exist")
        return data_directory

    def __check_for_tar(self, save_dir) -> bool:
        """
        Method checks for and unzips tar files within a specified directory.  It will also remove some of the additional
        files that are sometimes included in the tar file as well as partial downloads and the tar file itself.

        :param save_dir: The directory to search for tar files to unzip

        :return: True if a tar file was found to unzip, false otherwise.
        """

        found_files = False
        files = []
        files.extend(glob.glob(save_dir + "/*.tar"))

        for fname in files:
            if fname.endswith("tar"):
                try:
                    tar = tarfile.open(fname, "r:")
                    tar.extractall(save_dir)
                    tar.close()
                    found_files = True
                except Exception as e:
                    self._logger.error('Check for Tar error: %s', str(e))
                    self._logger.debug('Traceback: %s', traceback.format_exc())
                os.remove(fname)

        types = ('*.txt', '*.html', '*.json', '*.drmsrun', '*.qsub', '*.tar.*')
        files = []
        for ftype in types:
            files.extend(glob.glob(save_dir + "/" + ftype))

        for f in files:
            os.remove(f)

        return found_files

    @staticmethod
    def __get_wave_settings(waveband: WaveBand):
        if waveband is WaveBand.Wave_94:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(94 * u.AA)
            w_filter = 94
        elif waveband is WaveBand.Wave_131:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(131 * u.AA)
            w_filter = 131
        elif waveband is WaveBand.Wave_171:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(171 * u.AA)
            w_filter = 171
        elif waveband is WaveBand.Wave_193:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(193 * u.AA)
            w_filter = 193
        elif waveband is WaveBand.Wave_211:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(211 * u.AA)
            w_filter = 211
        elif waveband is WaveBand.Wave_304:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(304 * u.AA)
            w_filter = 304
        elif waveband is WaveBand.Wave_335:
            series = a.jsoc.Series('aia.lev1_euv_12s')
            s_filter = "aia.lev1_euv_12s"
            wave = a.Wavelength(335 * u.AA)
            w_filter = 335
        elif waveband is WaveBand.Wave_1600:
            series = a.jsoc.Series('aia.lev1_uv_24s')
            s_filter = "aia.lev1_uv_24s"
            wave = a.Wavelength(1600 * u.AA)
            w_filter = 1600
        elif waveband is WaveBand.Wave_1700:
            series = a.jsoc.Series('aia.lev1_uv_24s')
            s_filter = "aia.lev1_uv_24s"
            wave = a.Wavelength(1700 * u.AA)
            w_filter = 1700
        else:
            raise TypeError("Waveband is of the wrong type, cannot process this request.")

        return series, s_filter, wave, w_filter

    def download_records_for_range(self, start_time: datetime, end_time: datetime, waveband: WaveBand,
                                   cadence: timedelta) -> List[str]:
        """
        Downloads the AIA records for the specified date range, wavelength, and at the specified cadence rate starting
        from the start time of the date range.

        :param start_time:  :py:class:`datetime.datetime` The starting time for the range of data to obtain

        :param end_time: :py:class:`datetime.datetime` The ending time for the range of the data to obtain

        :param waveband: :py:class:`swdatatoolkit.datasources.WaveBand` The waveband of data to request

        :param cadence: :py:class:`datetime.timedelta` The cadence rate in minutes between observations to request

        :return: A list of successfully downloaded fits files in the storage directory where the data are located.
        """

        jsoc_series, series_filter, jsoc_wave, wave_filter = self.__get_wave_settings(waveband)
        sample_rate = a.Sample((cadence.total_seconds() / 60) * u.min)

        start_string = start_time.strftime('%Y-%m-%dT%H:%M:%S')
        end_string = end_time.strftime('%Y-%m-%dT%H:%M:%S')

        if jsoc_series is not None:
            has_complete = False
            attempt_count = 0
            while attempt_count < 5 and not has_complete:

                try:
                    client = jsoc.JSOCClient()
                    res = client.search(a.Time(start_string, end_string), jsoc_series, a.jsoc.Segment('image'),
                                        jsoc_wave, sample_rate, a.jsoc.Notify(self._drms_client_name))

                    requests = client.request_data(res, method='url-tar')

                    completed_list = []
                    if isinstance(requests, list):
                        for request in requests:
                            completed_list.append(self.__process_request(request))
                    elif isinstance(requests, drms.ExportRequest):
                        completed_list.append(self.__process_request(requests))

                    if all(completed_list):
                        has_complete = True
                except Exception as e:
                    self._logger.error('Attempt:%s - Download Chunk Error: %s - Series:%s', attempt_count, str(e),
                                       series_filter)
                    self._logger.debug('Download Chunk Error Traceback: %s - Series:%s', traceback.format_exc(),
                                       series_filter)
                time.sleep(10)
                attempt_count += 1

        files = []
        if has_complete:
            files.extend(glob.glob(self._data_directory + "/*.fits"))
        return files

    def download_records_for_response(self, response: JSOCResponse):
        """
        Downloads the records specified in the JSOCRespose object that is the result of a JSOC query.

        :param response: :py:class:`sunpy.net.jsoc.JSOCResponse` containing the results of a search at JSOC

        :return: A list of successfully downloaded fits files in the storage directory where the data are located.
        """

        has_complete = False
        attempt_count = 0
        while attempt_count < 5 and not has_complete:

            try:
                client = jsoc.JSOCClient()
                requests = client.request_data(response, method='url-tar')

                completed_list = []
                if isinstance(requests, list):
                    for request in requests:
                        completed_list.append(self.__process_request(request))
                elif isinstance(requests, drms.ExportRequest):
                    completed_list.append(self.__process_request(requests))

                if all(completed_list):
                    has_complete = True
            except Exception as e:
                self._logger.error('Attempt:%s - Download Response Chunk Error: %s', attempt_count, str(e))
                self._logger.debug('Download Response Chunk Error Traceback: %s', traceback.format_exc())
            time.sleep(10)
            attempt_count += 1

        files = []
        if has_complete:
            files.extend(glob.glob(self._data_directory + "/*.fits"))
        return files

    def get_records_for_range(self, start_time: datetime, end_time: datetime, waveband: WaveBand,
                              cadence: timedelta) -> JSOCResponse:
        """
        Queries the AIA records for the specified date range, wavelength, and at the specified cadence rate starting
        from the start time of the date range. It then returns the JSOCResponse object if successful, or None if not.

        :param start_time:  :py:class:`datetime.datetime` The starting time for the range of data to obtain

        :param end_time: :py:class:`datetime.datetime` The ending time for the range of the data to obtain

        :param waveband: :py:class:`swdatatoolkit.datasources.WaveBand` The waveband of data to request

        :param cadence: :py:class:`datetime.timedelta` The cadence rate in minutes between observations to request

        :return: :py:class:`sunpy.net.jsoc.JSOCResponse` containing the results of the search at JSOC or None if failed
        """

        jsoc_series, series_filter, jsoc_wave, wave_filter = self.__get_wave_settings(waveband)
        sample_rate = a.Sample((cadence.total_seconds() / 60) * u.min)

        start_string = start_time.strftime('%Y-%m-%dT%H:%M:%S')
        end_string = end_time.strftime('%Y-%m-%dT%H:%M:%S')

        try:
            client = jsoc.JSOCClient()
            res = client.search(a.Time(start_string, end_string), jsoc_series, a.jsoc.Segment('image'), jsoc_wave,
                                sample_rate, a.jsoc.Notify(self._drms_client_name))
            return res
        except Exception as e:
            self._logger.error('JSOC Query Error: %s - Series:%s', str(e), series_filter)
            self._logger.debug('JSOC Query Error Traceback: %s - Series:%s', traceback.format_exc(), series_filter)

        return None

    def __process_request(self, request: drms.ExportRequest):
        done = False
        try:
            try_count = 0
            while (not request.has_finished()) and (not request.has_failed()) and (try_count < 10):
                request.wait(sleep=30, timeout=120)
                try_count += 1

            if request.has_succeeded():
                tarurl = request.urls['url'][0]
                filename = request.tarfile.split('/')[-1]
                save_loc = os.path.join(self._data_directory, filename)
                try:
                    # make request and set timeout for no data to 120 seconds and enable streaming
                    with urllib.request.urlopen(tarurl, timeout=120) as resp:
                        # Open output file and make sure we write in binary mode
                        with open(save_loc + '.part', 'wb') as fh:
                            # walk through the request response in chunks of 1MiB
                            while not resp.closed:
                                chunk = resp.read(1024 * 1024)
                                if not chunk:
                                    break
                                # Write the chunk to the file
                                fh.write(chunk)

                        # Rename the file
                    os.rename(save_loc + '.part', save_loc)

                    self._logger.info("Data Chunk Downloaded for request:%s", request.id)
                    self.__check_for_tar(self._data_directory)
                    done = True
                except Exception as e:
                    self._logger.error('Download of request %s failed with error:%s ', request.id, str(e))
                    self._logger.debug('Download of request %s failed Traceback: %s ', request.id,
                                       traceback.format_exc())
            else:
                self._logger.info("Request was not successful failed with status %s", request.status)
        except Exception as e:
            self._logger.error('Request %s failed  with error:%s ', request.id, str(e))
            self._logger.debug('Request %s failed Traceback: %s - Series:%s', request.id,
                               traceback.format_exc())

        return done
