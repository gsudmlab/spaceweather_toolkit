"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import re
import logging
import traceback
import pandas as pd
import urllib.request
from logging import Logger
from pandas import DataFrame
from bs4 import BeautifulSoup
from collections import namedtuple
from collections import OrderedDict
from datetime import datetime, timedelta

Range = namedtuple('Range', ['start', 'end'])


class SOHOEPHINDownloader:
    """
    This class downloads 1 minute averaged data from the Electron Proton Helium Instrument (EPHIN) from
    `Comprehensive Suprathermal and Energetic Particle Analyzer (COSTEP) archive at Christian-Albrechts-University <http://ulysses.physik.uni-kiel.de/>`

    The output features are described in the `EPHIN documentation <http://ulysses.physik.uni-kiel.de/costep/doc/ephin_level3.pdf>`
    """

    def __init__(self, logger: Logger = None):
        """
        Constructor for the class.

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)
        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        self._base_url = "http://ulysses.physik.uni-kiel.de/costep/level3/l3i/1min/{}/"
        self._date_lim = datetime(1996, 1, 1)
        self._link_filter = re.compile("^[0-9]+_[0-9]+.l3i")

    def get_one_minute_avg_data(self, start_time: datetime, end_time: datetime) -> DataFrame:
        """
        Method that queries the repository for 1-minute averaged EPHIN data.

        :param start_time: :py:class:`datetime.datetime` The starting time for the range of data to obtain

        :param end_time: :py:class:`datetime.datetime` The ending time for the range of the data to obtain

        :return: :py:class:`pandas.DataFrame` containing the results of the search at OMNI Web or None if an error
            occurred while attempting to download.

        """

        self.__validate_parameters(start_time, end_time)
        try:
            date_partitions = OrderedDict(((start_time + timedelta(_)).strftime(r"%Y-%m"), None) for _ in
                                          range((end_time - start_time).days + 1)).keys()

            input_range = Range(start=start_time, end=end_time)
            date_format = "%Y_%j"

            df_list = []
            for year_mo_str in date_partitions:
                year = year_mo_str[0:4]
                month_url = self._base_url.format(year)

                files_list = []
                with urllib.request.urlopen(month_url, timeout=20) as resp:
                    soup = BeautifulSoup(resp.read(), features="html.parser")
                    for a in soup.find_all('a'):
                        if self._link_filter.match(a.text):
                            files_list.append(a.text)

                filtered_files = []
                for f in files_list:
                    end_idx = f.find('.')
                    f_start = datetime.strptime(f[0:end_idx], date_format)
                    f_end = f_start + timedelta(days=1)
                    file_range = Range(start=f_start, end=f_end)
                    latest_start = max(input_range.start, file_range.start)
                    earliest_end = min(input_range.end, file_range.end)
                    if latest_start < earliest_end:
                        filtered_files.append(f)

                for f in filtered_files:
                    df = self.__download_file(year, f)
                    if df is not None and not df.empty:
                        df_list.append(df)

            merged_df = pd.concat(df_list)
            mask = (merged_df.index > start_time) & (merged_df.index <= end_time)
            filtered_df = merged_df.loc[mask]
            filtered_df = filtered_df[~filtered_df.index.duplicated(keep='first')]
            return filtered_df
        except Exception as e:
            self._logger.error('SOHOEPHINDownloader.get_one_min_avg_data failed with: %s', str(e))
            self._logger.debug('SOHOEPHINDownloader.get_one_min_avg_data Traceback: %s', traceback.format_exc())

        return None

    def __validate_parameters(self, start_time: datetime, end_time: datetime) -> bool:

        if start_time < self._date_lim:
            raise ValueError("start_time cannot be before 1996/01/01")

        if end_time < start_time:
            raise ValueError("start_time cannot be after end_time")

        return True

    def __download_file(self, year: str, file_name: str) -> DataFrame:
        """
        Downloads the file from the source and parses it into a DataFrame

        :param year: :py:class:`str` The year directory to pull from

        :param file_name: :py:class:`str` The name of the file to pull and parse

        :return: :py:class:`pandas.DataFrame` | None The DataFrame is returned if downloaded and processed, else None
        """

        file_url = self._base_url.format(year) + file_name

        try:
            with urllib.request.urlopen(file_url, timeout=40) as resp:
                if resp.status == 200:
                    file_data = resp.read()

            if file_data is not None:
                file_data = file_data.decode().split('\n')
            else:
                self._logger.error("SOHOEPHINDownloader.__download_file failed to donload: %s", str(file_name))
                return

            cols = file_data[0][1:].strip().split()
            new_cols = ['date']
            new_cols.extend(cols[6:])

            rows = []
            for i in range(3, len(file_data)):
                row = file_data[i].split()
                if not len(row):
                    continue
                year = int(row[0])
                month = int(row[1])
                day = int(row[2])
                hour = int(row[4])
                minute = int(row[5])
                status = int(row[6])
                obs_time = datetime(year, month, day, hour, minute)
                new_row = [obs_time, status]
                new_row.extend([float(x) for x in row[7:]])
                rows.append(new_row)

            df = DataFrame(rows, columns=new_cols)
            df = df.set_index(['date'])
            return df
        except Exception as e:
            self._logger.error("SOHOEPHINDownloader.__download_file failed with error: %s", str(e))
            self._logger.debug("SOHOEPHINDownloader.__download_file failed traceback: %s", traceback.format_exc())
