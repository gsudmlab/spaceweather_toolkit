"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import logging
import requests
import traceback
import pandas as pd
from logging import Logger
from pandas import DataFrame
from bs4 import BeautifulSoup
from datetime import datetime


class OMNIWebDownloader:
    """
    This class is to download data from NASA's Goddard Space Flight Center `OMNIWeb <https://omniweb.gsfc.nasa.gov/>`.

    The class currently only implements downloading of the High resolution OMNIWeb 5-minute data.
    For information about the fields available in the returned data see the High Resolution Omni data set
    `info page <https://spdf.gsfc.nasa.gov/pub/data/omni/high_res_omni/hroformat.txt>`.
    """

    def __init__(self, logger: Logger = None):
        """
        Constructor for the class.

        :param logger: :py:class:`logging.Logger` The logging object used to record errors and other information logged
                                                by this class.  If None, then logging will be sent to stderr.
                                                (Default: ``None``)
        """

        if logger is None:
            self._logger = logging.getLogger()
            self._logger.addHandler(logging.StreamHandler())
        else:
            self._logger = logger

        self._date_lim = datetime(1982, 1, 1)
        self._base_url = "https://omniweb.gsfc.nasa.gov/cgi/nx1.cgi"

    def get_five_min_avg_data(self, start_time: datetime, end_time: datetime) -> DataFrame:
        """
        Method that queries OMNI Web for 5-minute OMNI data which includes protons above 10, 30 and 60 MeV.

        :param start_time: :py:class:`datetime.datetime`
            The starting time for the range of data to obtain

        :param end_time: :py:class:`datetime.datetime`
            The ending time for the range of the data to obtain

        :return: :py:class:`pandas.DataFrame`
            The parsed data placed into a DataFrame with the time_tag parsed into datetime objects and used as the
            index for the rows of the DataFrame. The returned DataFrame contain the results of the search at OMNI Web or
            None if an error occurred while attempting to download.
        """
        self._validate_parameters(start_time, end_time)
        date_format = "%Y-%j %H:%M"
        try:
            start_string = start_time.strftime('%Y%m%d%H')
            end_string = end_time.strftime('%Y%m%d%H')
            var_nums = [x for x in range(4, 49)]
            args = [('activity', 'retrieve'), ('res', '5min'), ('spacecraft', 'omni_5min_def'),
                    ('start_date', start_string), ('end_date', end_string)]
            args.extend([('vars', var_num) for var_num in var_nums])

            with requests.post(self._base_url, data=args, timeout=120) as resp:
                soup = BeautifulSoup(resp.content, features="html.parser")
                # The data returned is in a single string, so we find it and then split it.
                result = str(soup.find("pre")).split('\n')

            # Trim the param number from the description/column header for each parameter that was queried
            cols = [line[3:] for line in result[1:(len(var_nums) + 1)]]
            cols.append('time_tag')

            # Get the data table section of the result list
            data = result[(3 + len(var_nums)):-1]
            data_list = []
            for row in data:
                element_list = row.split()
                date_str = "{}-{} {}:{}".format(element_list[0], element_list[1], element_list[2], element_list[3])
                time_tag = datetime.strptime(date_str, date_format)
                element_list = element_list[4:]
                element_list.append(time_tag)
                data_list.append(element_list)

            if len(data_list):
                df = DataFrame(data_list, columns=cols)
                df = df.set_index('time_tag')

                mask = (df.index > start_time) & (df.index <= end_time)
                filtered_df = df.loc[mask]
                filtered_df = filtered_df[~filtered_df.index.duplicated(keep='first')]
                return filtered_df
            else:
                return None
        except Exception as e:
            self._logger.error('OMNIWebDownloader.get_five_min_avg_data failed with: %s', str(e))
            self._logger.debug('OMNIWebDownloader.get_five_min_avg_data Traceback: %s', traceback.format_exc())

        return None

    def _validate_parameters(self, start_time: datetime, end_time: datetime) -> bool:

        if start_time < self._date_lim:
            raise ValueError("start_time cannot be before 1982/01/01")

        if end_time < start_time:
            raise ValueError("start_time cannot be after end_time")
