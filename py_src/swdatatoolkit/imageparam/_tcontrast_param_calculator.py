"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
import numpy as np
from typing import Callable
from scipy.stats import kurtosis

from . import PatchSize
from ._base_calculator import BaseParamCalculator


###################################
# TContrastParamCalculator
###################################
class TContrastParamCalculator(BaseParamCalculator):
    """
    This class is for calculating the Tamura Contrast parameter over some patch of a 2D array.
    The calculation is performed in the following manner:

    .. math:: C = \\frac{\\sigma^{2}}{{\\mu_4}^{0.25}}

    where:

    - :math:`\\sigma^{2}` is the variance of the intensity values in the patch

    - :math:`\\mu_4` is kurtosis (4-th moment about the mean) of the intensity values in the patch

    This formula is an approximation proposed by Tamura et al. in "Textual Features Corresponding Visual
    Perception" and investigated in "On Using SIFT Descriptors for Image Parameter Evaluation"

    """

    def __init__(self, patch_size: PatchSize):
        """
        Constructor

        :param patch_size: The patch size to calculate the parameter over.
        :type patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
        """
        super().__init__(patch_size)
        self._elipses = 1e-32

    def _calc_func(self, data: numpy.ndarray) -> numpy.ndarray:
        '''
         :return: The Tcontrast parameter value for the patch passed in
        '''
        kurt_val = kurtosis(data, axis=None, nan_policy='omit')
        kurt_val = kurt_val + self._elipses  # To avoid divide by zero
        std_val = np.std(data)

        t_contrast = np.power(std_val, 2.0) / np.power(kurt_val, 0.25)

        if np.isnan(t_contrast):
            return 0.0

        return t_contrast

    @property
    def calc_func(self) -> Callable:
        return self._calc_func
