"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
import numpy as np

from typing import Callable, List

from . import PatchSize
from ._base_calculator import BaseParamCalculator

from .util import PeakDetector
from ..edgedetection import GradientCalculator


###################################
# TDirectionalityParamCalculator
###################################
class TDirectionalityParamCalculator(BaseParamCalculator):

    def __init__(self, patch_size: PatchSize, gradient_calculator: GradientCalculator, peak_detector: PeakDetector,
                 quantization_level: int):
        """
        Constructor

        :param patch_size: The patch size to calculate the parameter over.
        :param gradient_calculator: Calculator for gradient of pixel values in the image being processed.
        :param peak_detector: Object that finds the local maxima in an ordered series of values
        :param quantization_level: The quantization level for the continuous spectrum of the angles of gradients. This
            is the number of bins in a histogram of gradient angles.
        :type patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
        """
        super().__init__(patch_size)

        if gradient_calculator is None:
            raise TypeError("gradient_calculator cannot be None in TDirectionalityParamCalculator constructor.")
        if peak_detector is None:
            raise TypeError("peak_detector cannot be None in TDirectionalityParamCalculator constructor.")
        if quantization_level is None:
            raise TypeError("quantization_level cannot be None in TDirectionalityParamCalculator constructor.")

        self._gradient_calculator = gradient_calculator
        self._peak_detector = peak_detector
        self._quantization_level = quantization_level
        self._radius_threshold_percentage = 0.15
        self._insignificant_radius = 1e-4

    @property
    def calc_func(self) -> Callable:
        return self.__calc_directionality

    def incremental_array(self, first: int, last: int) -> List[int]:
        """
        This method generates all the integers between the first and last input inclusive

        :param first: The starting value in the generated list
        :param last: The last value in the generated list
        :return:
        """
        if first >= last:
            return None

        results = [first + i for i in range(0, int(last - first + 1))]
        return results

    def __calc_directionality(self, data: numpy.ndarray) -> float:
        '''
        :param data: :py:class:`numpy.ndarray`
        2D matrix representing some image
        :return: The TDirectionality parameter value for the patch passed in
        '''

        # Compute the gradient of the input image data in the polar coordinate system
        gradient = self._gradient_calculator.calculate_gradient_polar(data)

        gradient_theta = gradient.gx.flatten()
        gradient_radii = gradient.gy.flatten()

        # Clean the radii vector by setting all radii below a threshold to zero
        radiusThreshold = self._radius_threshold_percentage * np.max(gradient_radii)
        gradient_radii[gradient_radii > radiusThreshold] = 0.0

        # Clean the theta vector by setting theta values below a threshold to zero
        gradient_theta[np.absolute(gradient_theta) < self._insignificant_radius] = 0.0

        # Compute histogram of all angles of theta
        breaks = np.linspace(0, np.pi, num=self._quantization_level + 1, endpoint=True)
        breaks = np.array(breaks, dtype=np.float64)

        temp_hist = np.histogram(gradient_theta, bins=self._quantization_level, range=(0, np.pi))

        # Resize the first bin, since this bin is always disproportionately larger than others, and represents
        # the texture-less regions.
        hist_t = temp_hist[0]
        hist_t[0] = (hist_t[0] / 100)

        # Find peaks will give us the index of peaks sorted by their height, but we want them sorted by index
        peaks_index = self._peak_detector.find_peaks(hist_t)
        peaks_index.sort()

        number_of_peaks = len(peaks_index)
        if (number_of_peaks == 0):
            return 0.0

        # Find the middle points between the elements of the array of peaks.
        middle_points = [0] * (len(peaks_index) + 1)
        for i in range(1, len(peaks_index) - 1):
            middle_points[i] = int((peaks_index[i - 1] + peaks_index[i]) / 2)

        middle_points[len(peaks_index)] = len(breaks) - 1

        outer_sum = 0.0
        # Compute the double-sum of the Directionality formula
        for i in range(0, number_of_peaks):
            ## 1. Get the interval
            from_pt = middle_points[i]
            to_pt = middle_points[i + 1]
            ## 2. Create the incremental array for this interval
            inc = self.incremental_array(from_pt, to_pt)
            ## 3. Using the incremental array and the index of peak, compute the weights

            this_peak_index = peaks_index[i]
            inner_sum = 0
            if inc is not None:
                for j in range(0, len(inc) - 1, 1):
                    inner_sum += np.power(breaks[inc[j]] - breaks[this_peak_index], 2) * hist_t[inc[j]]

            outer_sum += inner_sum

        fDir = number_of_peaks * outer_sum
        normVal = np.sum(hist_t)
        fDir = fDir / normVal

        return fDir
