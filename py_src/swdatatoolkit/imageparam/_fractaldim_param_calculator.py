"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
import numpy as np
from typing import Callable
from sklearn.linear_model import LinearRegression

from . import PatchSize
from ._base_calculator import BaseParamCalculator
from ..edgedetection import CannyEdgeDetector


###################################
# FractalDimParamCalculator
###################################
class FractalDimParamCalculator(BaseParamCalculator):
    '''
    This class is designed to compute the <b>Fractal Dimension</b> parameter for
    each patch of the given <code>BufferedImage</code>. In this class, a <i>Box
    counting</i> approach known as <i>Minkowski�Bouligand Dimension</i> is
    implemented.
    '''

    def __init__(self, patch_size: PatchSize, edge_detector: CannyEdgeDetector):
        '''
        Constructor

        :param patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize` the size of the boxes by which the image will be processed.
        :param edge_detector: :py:class:`swdatatoolkit.edgedetection.CannyEdgeDetector` the algorithm to be used to extract the binary image of edges from the input image.
        '''

        super().__init__(patch_size)

        if (edge_detector is None):
            raise TypeError(
                "edge_detector cannot be null in FractalDimParamcalculator \nYou need to provide a valid edge detector, or use the other contructor.")

        self._edge_detector = edge_detector
        self._patch_size = patch_size
        self._epsilon = 1e-32

        '''
        To calculate box sizes we first calculate maxbox size based on the PatchSize.Enum based on the formula return 2 * round(np.sqrt(pSize)) if its 0, 
        then fractal dimension is going to be calculated for the entire image at once. So, in this case, we set the maxBoxSize to 64 pixel.

        We then calculate the splits using the max box size using the formula         

        results = [np.log2(a * 1.0)]
        results[0] = a
        i = 1
        while (a > 1):
            i+=1
            results[i] = a
            a = a / 2

        '''

        if self._patch_size == PatchSize.ONE:
            self._boxSizes = [2.0, 1.0]
        elif self._patch_size == PatchSize.FOUR or self._patch_size == PatchSize.SIXTEEN:
            self._boxSizes = [4.0, 2.0, 1.0]
        elif self._patch_size == PatchSize.THIRTY_TWO:
            self._boxSizes = [16.0, 8.0, 4.0, 2.0, 1.0]
        elif self._patch_size in [PatchSize.SIXTY_FOUR, PatchSize.ONE_TWENTY_EIGHT, PatchSize.TWO_FIFTY_SIX]:
            self._boxSizes = [32.0, 16.0, 8.0, 4.0, 2.0, 1.0]
        elif self._patch_size in [None, PatchSize.FIVE_TWELVE, PatchSize.TEN_TWENTY_FOUR, PatchSize.FULL]:
            self._boxSizes = [64.0, 32.0, 16.0, 8.0, 4.0, 2.0, 1.0]

        # self._maxBoxSize = pSize
        # self._boxSizes = self.getAllSplits(pSize)

    @property
    def calc_func(self) -> Callable:
        return self.__calc_fractal

    def __calc_fractal(self, data: numpy.ndarray) -> float:

        """
        Helper method that performs the fractal dim calculation for one patch.

        :param data: :py:class:`numpy.ndarray`
            2D matrix representing some image
        :return: The fractal dim parameter value for the patch passed in or zero if an error occurred

        """
        if (self._edge_detector == None):
            raise ValueError(
                "EdgeDetector cannot be null in FractalDimParamcalculator \nYou need to provide a valid edge detector, or use the other contructor.")

        image_h = data.shape[0]
        image_w = data.shape[1]

        """
        This method prepares the image for the box-counting method. It applies the
        given Edge Detection algorithm on the image (assigns white:255 to pixels
        representing the edges and black:0 to others), and then converts it into a 1D
        array and passes it to countBox method. If <code>null</code> was passed to
        the constructor of this class, then the given image will be considered
        binary, hence no preparation will be carried out.

        @param image
        @return a single double number as the Fractal Dimension of the given image.
        """

        """If no edge detector is provided, then use the given colors"""
        if self._edge_detector == None:
            colors = np.arange(128, 256)
            binaryImage = data
        else:
            colors = [255]  ## colors[0]: (B) background, colors[1]:(W) foreground
            binaryImage = self._edge_detector.get_edges(data)  # canny_output

        g = np.array(binaryImage).flatten().astype(np.float64)

        min_box = np.minimum(image_w, image_h)
        local_box_sizes = np.clip(self._boxSizes, None, min_box)
        counts = [0] * len(local_box_sizes)
        """Do counting for boxes of different sizes."""
        for i in range(0, len(local_box_sizes), 1):
            counts[i] = self._countBoxes(g, image_w, image_h, local_box_sizes[i], colors)
        """
        Find the regression slope for the points in the plot X: log(boxSize), Y:
        * log(counts)
        """

        # Create arrays for x and y values
        x = np.log(local_box_sizes)
        y = np.log(counts, where=(np.array(counts) > 0))

        # Fit the linear regression
        reg = LinearRegression().fit(x.reshape(-1, 1), y.reshape(-1, 1))
        slope = reg.coef_[0][0] if np.isfinite(reg.coef_[0][0]) else 0

        """
        The regression slope of such data is always negative, but we only care about
        the magnitude of the slope, hence the absolute value.
        """
        return abs(slope)

    def _countBoxes(self, image, imageW, imageH, boxSize, colors):
        if len(image) != imageW * imageH:
            raise ValueError("The given array does not match with the given width and height!")
        if boxSize > imageW or boxSize > imageH:
            raise ValueError("The given boxSize is larger than the patch on which the box counting should take place")

        subPatch = []
        x, y = 0, 0
        boxW, boxH = boxSize, boxSize
        done = False
        boxCounter = 0

        while not done:
            subPatch = self.getSubMatrix(image, imageW, imageH, x, y, boxW, boxH)

            for i in range(len(subPatch)):
                if subPatch[i] in colors:
                    # This subPatch contains a color in the list of colors, so it should be counted and
                    # there is no need to proceed any further.
                    boxCounter += 1
                    break

            # Move the box
            x += boxSize
            if x + boxSize > imageW:
                # If the remaining horizontal space is less than a box
                boxW = imageW % boxSize  # shrink the box horizontally to fit the remaining space
                if x >= imageW:
                    # Reset boxW and x
                    boxW = boxSize
                    x = 0
                    # Shift y
                    y += boxSize
                    if y + boxSize > imageH:
                        # If the remaining vertical space is less than a box
                        boxH = imageH % boxSize  # shrink the box vertically to fit the remaining space
                        if y >= imageH:
                            done = True  # done if the entire image is covered

        return boxCounter

    def getSubMatrix(self, data: numpy.ndarray, rowLength, colLength, xBox, yBox, boxW, boxH):
        if len(data) != rowLength * colLength:
            raise ValueError("the given matrix doesn't match with the given rowLength and colLength!")

        if boxW > rowLength or boxH > colLength:
            raise ValueError("The expected sub-matrix is bigger than the given matrix!")

        if boxW + xBox > rowLength or boxH + yBox > colLength:
            raise ValueError("The expected sub-matrix is out of the boundary of the given matrix!")

        subMatrix = []
        boxH_int = int(boxH)
        boxW_int = int(boxW)
        xBox_int = int(xBox)
        yBox_int = int(yBox)
        for i in range(boxH_int):
            for j in range(boxW_int):
                subMatrix.append(data[(xBox_int + j) + ((yBox_int + i) * rowLength)])

        return subMatrix
