"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
from typing import Callable
from scipy.stats import skew

from . import PatchSize
from ._base_calculator import BaseParamCalculator


###################################
# SkewnessParamCalculator
####################################
class SkewnessParamCalculator(BaseParamCalculator):
    """
    This class is for calculating the skewness parameter over some patch of a 2D array.

    See :py:class:`scipy.stats.skew` for additional information on the calculation for
    each cell.
    """

    def __init__(self, patch_size: PatchSize):
        """
        Constructor

        :param patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
            the patch size to calculate the parameter over.

        """
        super().__init__(patch_size)

    @staticmethod
    def _calc_func(data: numpy.ndarray) -> numpy.ndarray:
        '''
        Using scipy.stats library skew to calculate skewness of the image from the patch image
        :param data: :py:class:`numpy.ndarray`
        2D matrix representing some image
        :return: The skewness parameter value for the patch passed in
        '''
        val = skew(data, axis=None)
        return val

    @property
    def calc_func(self) -> Callable:
        return self._calc_func
