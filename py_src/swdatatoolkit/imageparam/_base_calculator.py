"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
import numpy as np
from typing import Callable
from abc import ABCMeta, abstractmethod

from . import PatchSize

class BaseParamCalculator(metaclass=ABCMeta):
    """
    This is a base abstract class for calculating parameters over some patch of a 2D array.

    """

    def __init__(self, patch_size: PatchSize):
        """
        Constructor

        :param patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
            the patch size to calculate the parameter over.

        """
        if patch_size is None:
            raise TypeError("PatchSize cannot be None in ParamCalculator constructor.")
        self._patch_size = patch_size

    @property
    @abstractmethod
    def calc_func(self) -> Callable:
        """
        This polymorphic property is designed to return the parameter calculation function to be applied to each
        patch of the input data.

        :return: :py:class:`typing.Callable` that is the parameter calculation function over a patch of a 2D array.
        """
        pass

    def calculate_parameter(self, data: numpy.ndarray) -> numpy.ndarray:
        """
        This polymorphic method is designed to compute some image parameter. The parameters shall be calculated by
        iterating over a given 2D in a patch by patch manner, calculating the parameter for the pixel values within that
        patch.

        :param data: :py:class:`numpy.ndarray`
            2D matrix representing some image

        :return: either a :py:class:`numpy.ndarray` of the parameter value for each patch within the original input
            :py:class:`numpy.ndarray`, or a single value representing the parameter value of the entire input
            :py:class:`numpy.ndarray`.

        """
        if data is None or not isinstance(data, np.ndarray):
            raise TypeError("Data cannot be None and must be of type numpy.ndarray")
        if self._patch_size is None:
            raise TypeError("PatchSize cannot be None in calculator.")

        image_h = data.shape[0]
        image_w = data.shape[1]

        if self._patch_size is PatchSize.FULL:
            return self.calc_func(data)

        p_size = self._patch_size.value

        if image_w % p_size != 0:
            raise ValueError("Width of data must be divisible by given patch size!")
        if image_h % p_size != 0:
            raise ValueError("Height of data must be divisible by given patch size!")

        div_h = image_h // p_size
        div_w = image_w // p_size

        vals = np.zeros((int(div_h), int(div_w)))
        for row in range(div_h):
            for col in range(div_w):
                start_r = p_size * row
                end_r = start_r + p_size
                start_c = p_size * col
                end_c = start_c + p_size
                vals[row, col] = self.calc_func(data[start_r:end_r, start_c:end_c])

        return vals
