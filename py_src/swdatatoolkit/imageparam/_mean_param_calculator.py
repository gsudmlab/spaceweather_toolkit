"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy as np
from typing import Callable

from . import PatchSize
from ._base_calculator import BaseParamCalculator


class MeanParamCalculator(BaseParamCalculator):
    """
    This class is for calculating the mean parameter over some patch of a 2D array.
    """

    def __init__(self, patch_size: PatchSize):
        """
        Constructor

        :param patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
            the patch size to calculate the parameter over.

        """

        super().__init__(patch_size)
        '''
        :return: The mean value for the patch passed in
        '''
        self._calc_func = np.mean

    @property
    def calc_func(self) -> Callable:
        return self._calc_func
