"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
import numpy as np
from typing import Callable

from . import PatchSize
from ._base_calculator import BaseParamCalculator


###################################
# UniformityParamCalculator
###################################
class UniformityParamCalculator(BaseParamCalculator):
    """
    This class is for calculating the uniformity parameter over some patch of a 2D array.


    """

    def __init__(self, patch_size: PatchSize, n_bins: int, min_val: float, max_val: float):
        """
        Constructor

        :param patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
            The patch size to calculate the parameter over.
        :param n_bins: int or sequence of scalars or str
            The number of bins to use when constructing the frequency histogram for each patch.
            If bins is an int, it defines the number of equal-width bins in the given range.
            If bins is a sequence, it defines a monotonically increasing array of bin edges,
            including the rightmost edge, allowing for non-uniform bin widths. See :py:class:`numpy.histogram`
            as it is used internally
        :param min_val: py:float
            The minimum value to use when constructing the frequency histogram for each patch.
            Values outside the range are ignored
        :param max_val: float
            The maximum value to use when constructing the frequency histogram for each patch.
            Values outside the range are ignored. The max_val must be greater than or equal to min_val.

        """
        super().__init__(patch_size)

        if n_bins is None:
            raise TypeError("n_bins cannot be None in UniformityParamCalculator constructor.")
        if min_val is None:
            raise TypeError("min_val cannot be None in UniformityParamCalculator constructor.")
        if max_val is None:
            raise TypeError("max_val cannot be None in UniformityParamCalculator constructor.")

        if min_val > max_val:
            raise ValueError("max_val cannot be less than min_val in UniformityParamCalculator constructor.")

        self._n_bins = n_bins
        self._range = (min_val, max_val)

    @property
    def calc_func(self) -> Callable:
        return self.__calc_uniformity

    def __calc_uniformity(self, data: numpy.ndarray) -> float:
        """
        Helper method that performs the uniformity calculation for one patch.

        :param data: :py:class:`numpy.ndarray`
            2D matrix representing some image
        :return: The uniformity parameter value for the patch passed in

        """

        hist, bin_edges = np.histogram(data, self._n_bins, range=self._range, density=False)
        image_h = data.shape[0]
        image_w = data.shape[1]

        '''
        Iterate over the normalized histogram of thisPatch (0 to nOfBins)
                Calculate uniformity for thisPatch:
                uniformity = SUM(p ^2)
        '''
        n_pix = float(image_w * image_h)
        sum = 0.0
        for i in range(len(hist)):
            count = hist[i]
            if count == 0:
                continue
            prob = hist[i] / n_pix
            sum += np.power(prob, 2)

        return sum
