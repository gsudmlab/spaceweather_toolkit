"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import numpy
import numpy as np
from typing import Callable

from . import PatchSize
from ._base_calculator import BaseParamCalculator


###################################
# RelativeSmoothnessParamCalculator
###################################
class RelativeSmoothnessParamCalculator(BaseParamCalculator):
    """
    This class is for calculating the relative smoothness parameter over some patch of a 2D array.


    """

    def __init__(self, patch_size: PatchSize):
        """
        Constructor

        :param patch_size: :py:class:`swdatatoolkit.imageparam.PatchSize`
            the patch size to calculate the parameter over.

        """
        super().__init__(patch_size)

    @staticmethod
    def _calc_func(data: numpy.ndarray) -> numpy.ndarray:
        '''
        Compute the variance of the above image patch,by using the variance we'll calculate the smoothness
        :param data: :py:class:`numpy.ndarray`
            2D matrix representing some image
        :return: The smoothness parameter value for the patch passed in
        '''
        val = np.var(data)
        val = 1 - (1.0 / (1 + val))
        return val

    @property
    def calc_func(self) -> Callable:
        return self._calc_func
