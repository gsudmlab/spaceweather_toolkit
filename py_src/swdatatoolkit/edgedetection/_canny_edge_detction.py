"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.

Source code in this file is adapted from the CannyEdge Detector implementation in openimaj
http://openimaj.org/apidocs/src-html/org/openimaj/image/processing/edges/CannyEdgeDetector.html
And the cannyEdgeDetector from
https://github.com/FienSoP/canny_edge_detector/blob/master/canny_edge_detector.py

"""

from . import BaseEdgeDetector

from scipy.ndimage import convolve

import numpy
import numpy as np

import scipy
from scipy import ndimage


class CannyEdgeDetector(BaseEdgeDetector):
    """
    Canny edge detector. Performs the following steps:
    <ol>
    <li>Gaussian blur with std.dev. sigma</li>
    <li>Horizontal and vertical edge detection with Sobel operators</li>
    <li>Non-maximum suppression</li>
    <li>Hysteresis thresholding</li>
    </ol>
        
    The upper and lower thresholds for the hysteresis thresholding can be
    specified manually or automatically chosen based on the histogram of the edge
    magnitudes.

    """

    def __init__(self, low_threshold: float = 0.02, high_threshold: float = 0.08, kernel_size: int = 5,
                 sigma: float = 1.5):
        """
        Constructor for the class CannyEdgeDetector

        @param low_threshold:        The low threshold for hysteresis.

        @param high_threshold:       The high threshold for hysteresis.

        @param kernel_size:          The size of the Gaussian kernel

        @param sigma:                The standard deviation of the Gaussian convolution kernel
                                    used to smooth the source image prior to gradient
                                    calculation.
        """
        self._low_threshold = low_threshold
        self._high_threshold = high_threshold
        self._sigma = sigma

        if self._low_threshold <= 0:
            raise ValueError("The low hysteresis threshold must be greater than zero.")

        if self._high_threshold <= 0:
            raise ValueError("The high hysteresis threshold must be greater than zero.")

        if self._sigma <= 0:
            raise ValueError("Sigma must be greater than zero.")

        self._gaussian_kernel = self.__make_kernel(kernel_size, sigma)
        # self._grad_calculator = GradientCalculator('sobel')
        self._strong_pix = 255
        self._weak_pix = 75

    def get_edges(self, sourceImage: numpy.ndarray) -> numpy.ndarray:
        """

        :param sourceImage: :py:class:`numpy.ndarray`
        :return:

        """
        ##Gaussian Convolution
        img_smoothed = convolve(sourceImage, self._gaussian_kernel)
        ##Sobel Gradient of Gaussian Results
        # grad = self._grad_calculator.calculate_gradient_polar(img_smoothed)
        grad, theta = self.__sobel_filters(img_smoothed)
        ##Non-Max Suppression
        # non_max_img = self.__non_max_suppression(grad.gy, grad.gx)
        non_max_img = self.__non_max_suppression(grad, theta)
        ##Hysteresis
        img_final = self.__hysteresis(non_max_img)
        return img_final

    def __make_kernel(self, kernel_size: int = 5, sigma: float = 1) -> numpy.ndarray:

        """
        Construct a zero-mean Gaussian with the specified standard deviation.
        :param kernel_size: The number of pixels to include in the convolution
        :param sigma: The standard deviation of the Gaussian
        :return:

        """
        size = kernel_size // 2
        x, y = np.mgrid[-size:size + 1, -size:size + 1]
        normal = 1 / (2.0 * np.pi * sigma ** 2)
        g = np.exp(-((x ** 2 + y ** 2) / (2.0 * sigma ** 2))) * normal
        return g

    @staticmethod
    def __sobel_filters(img):
        Kx = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], np.float32)
        Ky = np.array([[1, 2, 1], [0, 0, 0], [-1, -2, -1]], np.float32)

        Ix = convolve(img, Kx)
        Iy = convolve(img, Ky)

        G = np.hypot(Ix, Iy)
        g_max = G.max()
        if g_max > 0.0:
            G = G / g_max * 255
        theta = np.arctan2(Iy, Ix)

        return (G, theta)

    @staticmethod
    def __non_max_suppression(grad, theta) -> np.ndarray:

        rows, cols = grad.shape
        result = np.zeros((rows, cols), dtype=np.int32)
        angle = theta * 180.0 / np.pi
        angle[angle < 0] += 180

        for row in range(1, rows - 1):
            for col in range(1, cols - 1):
                try:
                    q = 255
                    r = 255
                    angle_val = angle[row, col]
                    # angle 0
                    if (0 <= angle_val < 22.5) or (157.5 <= angle_val <= 180):
                        q = grad[row, col + 1]
                        r = grad[row, col - 1]
                    # angle 45
                    elif 22.5 <= angle_val < 67.5:
                        q = grad[row + 1, col - 1]
                        r = grad[row - 1, col + 1]
                    # angle 90
                    elif 67.5 <= angle_val < 112.5:
                        q = grad[row + 1, col]
                        r = grad[row - 1, col]
                    # angle 135
                    elif 112.5 <= angle_val < 157.5:
                        q = grad[row - 1, col - 1]
                        r = grad[row + 1, col + 1]

                    # If max in the angle direction then set result to this position
                    if (grad[row, col] >= q) and (grad[row, col] >= r):
                        result[row, col] = grad[row, col]

                except IndexError as e:
                    pass

        return result

    def __threshold(self, img):

        high_threshold = img.max() * self._high_threshold
        low_threshold = high_threshold * self._low_threshold

        row, col = img.shape
        result = np.zeros((row, col), dtype=np.int32)

        strong_row, strong_col = np.where(img >= high_threshold)
        weak_row, weak_col = np.where((img <= high_threshold) & (img >= low_threshold))
        result[strong_row, strong_col] = self._strong_pix
        result[weak_row, weak_col] = self._weak_pix

        return result

    def __hysteresis(self, img):

        img = self.__threshold(img)

        rows, cols = img.shape

        strong = self._strong_pix

        strong_pix = img == self._strong_pix
        weak_pix = img == self._weak_pix
        f1 = np.asarray([[1, 1, 1], [1, 0, 1], [1, 1, 1]])
        cond_max = scipy.ndimage.maximum_filter(img, footprint=f1, mode='constant', cval=-np.inf)

        results = np.zeros(img.shape)
        results[strong_pix | (weak_pix & (cond_max == strong))] = strong

        # for row in range(1, rows - 1):
        #     for col in range(1, cols - 1):
        #         if img[row, col] == self._weak_pix:
        #             try:
        #                 if ((img[row + 1, col - 1] == strong) or (img[row + 1, col] == strong) or (
        #                         img[row + 1, col + 1] == strong) or (img[row, col - 1] == strong) or (
        #                         img[row, col + 1] == strong) or (img[row - 1, col - 1] == strong) or (
        #                         img[row - 1, col] == strong) or (img[row - 1, col + 1] == strong)):
        #                     img[row, col] = strong
        #                 else:
        #                     img[row, col] = 0
        #             except IndexError as e:
        #                 pass

        return results
