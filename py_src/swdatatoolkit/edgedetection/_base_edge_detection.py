"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2023 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import numpy
from abc import ABCMeta, abstractmethod

class BaseEdgeDetector(metaclass=ABCMeta):
    """
    This is a base abstract class for calculating edges over some 2D array.

    """

    @abstractmethod
    def get_edges(self, sourceImage: numpy.ndarray) -> numpy.ndarray:
        """
            This method extracts edges of the given image by following performing the following steps:
            1.) Compute gradients
            2.) Perform Hysterisis
            3.) Thresholding

        :param sourceImage: The source image whose edges are to be extracted
        :return: A :py:class:`numpy.ndarray` that is a binary array representing the edges and background of the input
        """
        pass
