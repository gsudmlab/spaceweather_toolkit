"""
 * swdatatoolkit, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2024 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This code is adapted from computation modules taken from Monica Bobra's Python implementation of the SHARP JSOC code.
 * Source of parameter modules: https://github.com/mbobra/SHARPs/blob/master/calculate_sharpkeys.py
 *
"""
import math
import numpy as np
import numpy.ma as ma

from pandas import DataFrame


class GammaCalculator:
    """
    Class that calculates the MEANGAM (mean inclination angle, gamma, in degrees) and MEANGAM_ERR (the error estimate
    of the mean inclination angle) parameters on an input 2D magnetic field array.

    Native units of atan(x) are in radians so the values are converted to degress by multiplying by 180/PI

    Error analysis calculations are done in radians since the derivatives are calculated in units of radians.  The
    caluclated value is multiplied by 180/PI at the end of the calculation to provide a consistent unit with the gamma
    calculation.
    """

    def calc(self, bz_mag_vals: ma.MaskedArray, bh_mag_vals: ma.MaskedArray, bz_err_vals: ma.MaskedArray,
             bh_err_vals: ma.MaskedArray) -> DataFrame:
        """
        Calculates the MEANGAM and MEANGAM_ERR on the Bz radial magnetic field array and the Bh horizontal magnetic
        field array.

        :param bz_mag_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the radial magnetic field, with rows representing observations at different latitude and
            columns representing observations at different longitude. It is assumed that values that should not be
            included in the calculation have been masked when constructing the :py:class:`numpy.ma.MaskedArray` input.

        :param bh_mag_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the horizontal magnetic field, with rows representing observations at different latitude and
            columns representing observations at different longitude. This 2D array is calculated by the
            :class:`HorizontalFieldCalculator`. It is assumed that values that should not be included in the calculation
            have been masked when constructing the :py:class:`numpy.ma.MaskedArray` input.

        :param bz_err_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the computed uncertainty (standard deviation) of the radial magnetic field , with rows
            representing observations at different latitude and columns representing observations at different longitude.
            It is assumed that values that should not be included in the calculation have been masked when constructing
            the :py:class:`numpy.ma.MaskedArray` input. It is also assumed that the mask of the error array matches the
            mask of the corresponding magnetic field input.

        :param bh_err_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the computed uncertainty (standard deviation) of the horizontal magnetic field , with rows
            representing observations at different latitude and columns representing observations at different longitude.
            This 2D array is calculated by the :class:`HorizontalFieldCalculator`. It is assumed that values that should
            not be included in the calculation have been masked when constructing the :py:class:`numpy.ma.MaskedArray`
            input. It is also assumed that the mask of the error array matches the mask of the corresponding magnetic
            field input.

        :return: :py:class:`pandas.DataFrame` The returned values of this calculation
        """

        if bz_mag_vals is None:
            raise TypeError("The Bz mag array cannot be none!")

        if bh_mag_vals is None:
            raise TypeError("The Bh mag array cannot be none!")

        if bz_err_vals is None:
            raise TypeError("The Bz error array cannot be none!")

        if bh_err_vals is None:
            raise TypeError("The Bh error array cannot be none!")

        ny_bz, nx_bz = bz_mag_vals.shape
        ny_bh, nx_bh = bh_mag_vals.shape
        ny_err, nx_err = bz_err_vals.shape
        ny_bh_err, nx_bh_err = bh_err_vals.shape

        if not (ny_bz == ny_bh == ny_err == ny_bh_err and nx_bz == nx_bh == nx_err == nx_bh_err):
            raise ValueError("All input arrays must be the same shape!")

        bz_mask = ma.getmask(bz_mag_vals)
        h_mask = ma.getmask(bh_mag_vals)
        h_low_mask = bh_mag_vals < 100
        z_low_mask = bz_mag_vals == 0
        combined_mask = bz_mask | h_mask | h_low_mask | z_low_mask

        masked_bz_err = ma.masked_array(bz_err_vals, mask=combined_mask)
        masked_bh_err = ma.masked_array(bh_err_vals, mask=combined_mask)
        masked_bh = ma.masked_array(bh_mag_vals, mask=combined_mask)
        masked_bz = ma.masked_array(bz_mag_vals, mask=combined_mask)
        sum_gamma = ma.sum(ma.fabs(ma.arctan(masked_bh / ma.fabs(masked_bz))))
        sum_gamma = sum_gamma * (180.0 / math.pi)
        """
        From JSOC calculation of error for gamma.
               err += (1/
                          (1+(
                               (bh[j * nx + i]*bh[j * nx + i])/
                               (bz[j * nx + i]*bz[j * nx + i])
                               )
                          )
                       )
                         *
                       (1/
                          (1+(
                               (bh[j * nx + i]*bh[j * nx + i])/
                               (bz[j * nx + i]*bz[j * nx + i])
                              )
                           )
                       )
                       *
                       (
                           (
                               (bh_err[j * nx + i]*bh_err[j * nx + i])/
                               (bz[j * nx + i]*bz[j * nx + i])
                           ) +
                            (
                               (bh[j * nx + i]*bh[j * nx + i]*bz_err[j * nx + i]*bz_err[j * nx + i])/
                               (bz[j * nx + i]*bz[j * nx + i]*bz[j * nx + i]*bz[j * nx + i])
                            ) 
                       );
        NumpPy implementation below.        
        """

        bz_squared = ma.power(masked_bz, 2)
        bh_squared = ma.power(masked_bh, 2)
        bz_err_squared = ma.power(masked_bz_err, 2)
        bh_err_squared = ma.power(masked_bh_err, 2)

        bh_sq_div_bz_sq = (bh_squared / bz_squared) + 1.0
        half_one = 1.0 / bh_sq_div_bz_sq
        half_one = ma.power(half_one, 2)

        bh_err_div_bz = bh_err_squared / bz_squared
        bh_mult_bz_err = bh_squared * bz_err_squared
        half_two = bh_err_div_bz + (bh_mult_bz_err / ma.power(masked_bz, 4))
        err = ma.sum(half_one * half_two)
        count_pix = np.count_nonzero(combined_mask == 0)

        mean_gamma = sum_gamma / count_pix
        mean_gamma_err = (np.sqrt(err) / count_pix) * (180 / math.pi)

        cols = ['MEANGAM', 'MEANGAM_ERR']
        data = [[mean_gamma, mean_gamma_err]]
        result = DataFrame(data, columns=cols)
        return result
