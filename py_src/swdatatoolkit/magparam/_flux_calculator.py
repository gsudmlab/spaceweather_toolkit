"""
 * swdatatoolkit, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2024 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This code is adapted from computation modules taken from Monica Bobra's Python implementation of the SHARP JSOC code.
 * Source of parameter modules: https://github.com/mbobra/SHARPs/blob/master/calculate_sharpkeys.py
 *
"""
import numpy as np
import numpy.ma as ma
from numpy import ndarray

from pandas import DataFrame


class FluxCalculator:
    """
    Class that calculates the USFLUX (total unsigned flux in units of G/cm^2) and FLUXIMB (the imbalance in flux between
    positive and negative polarities in units of G/cm^2) parameters on an input 2D magnetic field array. The class
    assumes the input arrays of magnetic field are from CEA magnetic field rectilinear components Br, Btheta, and
    Bphi or line-of-sight magnetogram.

    The unsigned flux is calculated as:

    flux = surface integral [(vector Bz) dot (normal vector)],
         = surface integral [(magnitude Bz)*(magnitude normal)*(cos theta)].

    However, when using the BZ field that is radial, it is assumed that cos theta = 1.
    Therefore, the pixels only need to be corrected for the projection. Thus, this class assumes the CEA projection
    has been performed on the input array.

    To convert G to G*cm^2, we simply multiply by the number of square centimeters per pixel.
    As an order of magnitude estimate, we can assign 0.5 to CDELT1 and 722500m/arcsec to (RSUN_REF/RSUN_OBS).
    (Gauss/pix^2)(CDELT1)^2(RSUN_REF/RSUN_OBS)^2(100.cm/m)^2
    =Gauss*cm^2

    Note that the value of CDELT1 in hmi.sharp_cea_720s is in units of degrees per pixel, but the function requests
    arcseconds per pixel element. The following calculation converts CDELT1 into arcseconds to produce the variable
    cdelt1_arcseconds in units of arseconds per pixel:

    cdelt1_arcsec = (math.atan((rsun_ref*cdelt1*radsindeg)/(dsun_obs)))*(1/radsindeg)*(3600.)

    Note: dsun_obs = the distance to the sun
          rsun_ref = the reference radius of the sun in meters
    """

    def calc(self, bz_mag_vals: ma.MaskedArray, bz_err_vals: ma.MaskedArray, cdelt1_arcsec: float, rsun_ref: float,
             rsun_obs: float) -> DataFrame:
        """
        Calculates the USFLUX, USFLUX_ERR, and FLUXIMB on the Bz radial magnetic field array.

        :param bz_mag_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the radial magnetic field, with rows representing observations at different latitude and
            columns representing observations at different longitude. It is assumed that values that should not be
            included in the calculation have been masked when constructing the :py:class:`numpy.ma.MaskedArray` input.

        :param bz_err_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the computed uncertainty (standard deviation) of the radial magnetic field , with rows
            representing observations at different latitude and columns representing observations at different longitude.
            It is assumed that values that should not be included in the calculation have been masked when constructing
            the :py:class:`numpy.ma.MaskedArray` input. It is also assumed that the mask of the error array matches the
            mask of the corresponding magnetic field input.

        :param cdelt1_arcsec: :py:class:`float`
            Arcseconds longitude per array element

        :param rsun_ref: :py:class:`float`
            Radius of Sun in m, agreed upon standard

        :param rsun_obs: :py:class:`float`
            Angular radius of Sun in arc-sec

        :return: :py:class:`pandas.DataFrame` The returned values of this calculation
        """

        if bz_mag_vals is None:
            raise TypeError("The mag array cannot be none!")

        if bz_err_vals is None:
            raise TypeError("The error array cannot be none!")

        ny_bz, nx_bz = bz_mag_vals.shape
        ny_err, nx_err = bz_err_vals.shape

        if not (ny_bz == ny_err and nx_bz == nx_err):
            raise ValueError("All input arrays must be the same shape!")

        err_sum = ma.sum(ma.power(bz_err_vals, 2))

        cdelt_squared = np.power(cdelt1_arcsec, 2)
        sq_cm_per_pix_squared = np.power((rsun_ref / rsun_obs), 2)
        abs_flux_err = np.sqrt(err_sum) * np.fabs(cdelt_squared * sq_cm_per_pix_squared * 10000)

        result = self.__calc_non_err_terms(bz_mag_vals, cdelt1_arcsec, rsun_ref, rsun_obs)
        col = ['USFLUX_ERR']
        data = [abs_flux_err]
        result_2 = DataFrame(data, columns=col)
        result = result.join(result_2)
        return result

    def calc_los(self, los_mag_vals: ma.MaskedArray, cdelt1_arcsec: float, rsun_ref: float,
                 rsun_obs: float) -> DataFrame:
        """
        Calculates the USFLUX and FLUXIMB on the line of sight magnetic field array.

        :param los_mag_vals: :py:class:`numpy.ndarray`
            A 2D array of the line of sight magnetic field, with rows representing observations at different latitude
            and columns representing observations at different longitude. It is assumed that values that should not be
            included in the calculation have been masked when constructing the :py:class:`numpy.ma.MaskedArray` input.

        :param cdelt1_arcsec: :py:class:`float`
            Arcseconds longitude per array element

        :param rsun_ref: :py:class:`float`
            Radius of Sun in m, agreed upon standard

        :param rsun_obs: :py:class:`float`
            Angular radius of Sun in arc-sec

        :return: :py:class:`pandas.DataFrame` The returned values of this calculation
        """

        if los_mag_vals is None:
            raise TypeError("The mag array cannot be none!")

        results = self.__calc_non_err_terms(los_mag_vals, cdelt1_arcsec, rsun_ref, rsun_obs)
        return results

    @staticmethod
    def __calc_non_err_terms(mag_vals: ma.MaskedArray, cdelt1: float, rsun_ref: float,
                             rsun_obs: float) -> DataFrame:

        val_sum = ma.sum(mag_vals)
        abs_sum = ma.sum(ma.fabs(mag_vals))

        cdelt_squared = np.power(cdelt1, 2)
        sq_cm_per_pix_squared = np.power((rsun_ref / rsun_obs), 2)
        abs_flux = abs_sum * cdelt_squared * sq_cm_per_pix_squared * 10000
        flux_imb = val_sum * cdelt_squared * sq_cm_per_pix_squared * 10000

        cols = ['USFLUX', 'FLUXIMB']
        data = [[abs_flux, flux_imb]]
        result = DataFrame(data, columns=cols)
        return result
