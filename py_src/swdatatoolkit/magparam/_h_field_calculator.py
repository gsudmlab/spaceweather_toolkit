"""
 * swdatatoolkit, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2024 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This code is adapted from computation modules taken from Monica Bobra's Python implementation of the SHARP JSOC code.
 * Source of parameter modules: https://github.com/mbobra/SHARPs/blob/master/calculate_sharpkeys.py
 *
"""

import numpy as np
import numpy.ma as ma
from typing import Tuple
from numpy import ndarray


class HorizontalFieldCalculator:
    """
    Class that calculates the Bh, or the horizontal field in units of Gauss, and returns the array containing these
    values at each pixel location. The horizontal error, or computed uncertainty (standard deviation) of the field is
    also calculated for each pixel location.  These arrays are utilized in other calculations, such as the
    :class:`GammaCalculator`.

    The class assumes the input arrays of magnetic field are from CEA magnetic field rectilinear components Btheta, and
    Bphi.
    """

    def calc(self, bx_mag_vals: ndarray, by_mag_vals: ndarray, bx_err: ndarray, by_err: ndarray) -> Tuple[
        ma.MaskedArray, ma.MaskedArray]:
        """
        Calculates the Bh (horizontal field) in units of Gauss and Bh_error (horizontal error).

        :param bx_mag_vals: :py:class:`numpy.ndarray`
            A 2D array of the westward Phi component of the CEA vector magnetic field, with rows representing
            observations at different latitude and columns representing observations at different longitude.

        :param by_mag_vals: :py:class:`numpy.ndarray`
            A 2D array of the southward Theta component of the CEA vector magnetic field, with rows representing
            observations at different latitude and columns representing observations at different longitude.

        :param bx_err: :py:class:`numpy.ndarray`
            A 2D array of the computed uncertainty (standard deviation) of the Phi component of the CEA vector magnetic
            field, with rows representing observations at different latitude and columns representing observations at
            different longitude.

        :param by_err: :py:class:`numpy.ndarray`
            A 2D array of the computed uncertainty (standard deviation) of the Theta component of the CEA vector
            magnetic field, with rows representing observations at different latitude and columns representing
            observations at different longitude.

        :return: :py:class:`Tuple` A tuple of two :py:class:`numpy.ma.MaskedArray` objects that represent the 2D arrays
            of Bh (horizontal field) in units of Gauss and Bh_error (horizontal error) respectively.
        """

        if bx_mag_vals is None:
            raise TypeError("The Phi component mag array cannot be none!")

        if bx_err is None:
            raise TypeError("The Phi component error array cannot be none!")

        if by_mag_vals is None:
            raise TypeError("The Theta component mag array cannot be none!")

        if by_err is None:
            raise TypeError("The Theta component error array cannot be none!")

        ny_bx, nx_bx = bx_mag_vals.shape
        ny_bx_err, nx_bx_err = bx_err.shape

        ny_by, nx_by = by_mag_vals.shape
        ny_by_err, nx_by_err = by_err.shape

        if not (ny_bx == ny_bx_err == ny_by == ny_by_err and nx_bx == nx_bx_err == nx_by == nx_by_err):
            raise ValueError("All input arrays must be the same shape!")

        bx_nan_mask = np.isnan(bx_mag_vals)
        by_nan_mask = np.isnan(by_mag_vals)

        masked_bx = ma.masked_array(bx_mag_vals, mask=bx_nan_mask)
        masked_by = ma.masked_array(by_mag_vals, mask=by_nan_mask)

        bx_squared = ma.power(masked_bx, 2.0)
        by_squared = ma.power(masked_by, 2.0)

        bh = ma.sqrt(bx_squared + by_squared)

        masked_bx_err = ma.masked_array(bx_err, bx_nan_mask)
        masked_by_err = ma.masked_array(by_err, by_nan_mask)

        bx_err_squared = ma.power(masked_bx_err, 2.0)
        by_err_squared = ma.power(masked_by_err, 2.0)

        bh_err = ma.sqrt(bx_squared * bx_err_squared + by_squared * by_err_squared) / bh

        return bh, bh_err
