"""
 * swdatatoolkit, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2024 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * This code is adapted from computation modules taken from Monica Bobra's Python implementation of the SHARP JSOC code.
 * Source of parameter modules: https://github.com/mbobra/SHARPs/blob/master/calculate_sharpkeys.py
 *
"""
import math
import numpy as np
import numpy.ma as ma

from pandas import DataFrame


class HorizontalDerivativeCalculator:
    """
    Class that calculates the MEANGBH (mean gradient of the horizontal magnetic field in units of G/Mm) on an input 2D
    magnetic field array. The class assumes the input array Bh of horizontal magnetic field is calculated the from CEA
    magnetic field rectilinear components Btheta and Bphi using the :class:`HorizontalFieldCalculator`.

    """

    def calc(self, bh_mag_vals: ma.MaskedArray, bh_err_vals: ma.MaskedArray, cdelt1: float, rsun_ref: float,
             rsun_obs: float) -> DataFrame:
        """
        Calculates the MEANGBH and MEANGBH on the Bh horizontal magnetic field array. The derivative of the horizontal
        field is calculated as sqrt[(dBh/dx)^2 + (dBh/dy)^2]. It assumes that the native units of the input arrays are
        in Gauss/pixel.

        The conversion from Gauss/pixel to Gauss/Mm has not been calculated in the returned values to do so use:
        Assume that the units of the magnetic field Bh are in Gauss, and the units of length for dx and dy are in
        pixels. So, to get Gauss/Mm = (Gauss/pix)(pix/arcsec)(arcsec/meter)(meter/Mm)
                                    = (Gaus/pix)(1/cdelt1)(rsun_obs/rsun_ref)(1,000,000)
                                    = Gauss/Mm

        :param bh_mag_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the horizontal magnetic field, with rows representing observations at different latitude and
            columns representing observations at different longitude. This 2D array is calculated by the
            :class:`HorizontalFieldCalculator`. It is assumed that values that should not be included in the calculation
            have been masked when constructing the :py:class:`numpy.ma.MaskedArray` input.

        :param bh_err_vals: :py:class:`numpy.ma.MaskedArray`
            A 2D array of the computed uncertainty (standard deviation) of the horizontal magnetic field , with rows
            representing observations at different latitude and columns representing observations at different longitude.
            This 2D array is calculated by the :class:`HorizontalFieldCalculator`. It is assumed that values that should
            not be included in the calculation have been masked when constructing the :py:class:`numpy.ma.MaskedArray`
            input. It is also assumed that the mask of the error array matches the mask of the corresponding magnetic
            field input.

        :param cdelt1: :py:class:`float`
            Degrees longitude per array element

        :param rsun_ref: :py:class:`float`
            Radius of Sun in m, agreed upon standard

        :param rsun_obs: :py:class:`float`
            Angular radius of Sun in arc-sec

        :return: :py:class:`pandas.DataFrame` The returned values of this calculation
        """

        if bh_mag_vals is None:
            raise TypeError("The Bh mag array cannot be none!")

        if bh_err_vals is None:
            raise TypeError("The Bh error array cannot be none!")

        ny_bh, nx_bh = bh_mag_vals.shape
        ny_bh_err, nx_bh_err = bh_err_vals.shape

        if not (ny_bh == ny_bh_err and nx_bh == nx_bh_err):
            raise ValueError("All input arrays must be the same shape!")

        derx_bh = np.zeros([ny_bh, nx_bh])
        dery_bh = np.zeros([ny_bh, nx_bh])
        err_term1 = np.zeros([ny_bh, nx_bh])
        err_term2 = np.zeros([ny_bh, nx_bh])

        # brute force method of calculating the derivative d/dx (no consideration for edges)
        for i in range(1, nx_bh - 1):
            for j in range(0, ny_bh):
                derx_bh[j, i] = (bh_mag_vals[j, i + 1] - bh_mag_vals[j, i - 1]) * 0.5
                err_term1[j, i] = (((bh_mag_vals[j, i + 1] - bh_mag_vals[j, i - 1]) * (
                        bh_mag_vals[j, i + 1] - bh_mag_vals[j, i - 1])) * (
                                           bh_err_vals[j, i + 1] * bh_err_vals[j, i + 1] + bh_err_vals[j, i - 1] *
                                           bh_err_vals[j, i - 1]))

        # brute force method of calculating the derivative d/dy (no consideration for edges) */
        for i in range(0, nx_bh):
            for j in range(1, ny_bh - 1):
                dery_bh[j, i] = (bh_mag_vals[j + 1, i] - bh_mag_vals[j - 1, i]) * 0.5
                err_term2[j, i] = (((bh_mag_vals[j + 1, i] - bh_mag_vals[j - 1, i]) * (
                        bh_mag_vals[j + 1, i] - bh_mag_vals[j - 1, i])) * (
                                           bh_err_vals[j + 1, i] * bh_err_vals[j + 1, i] + bh_err_vals[j - 1, i] *
                                           bh_err_vals[j - 1, i]))

        # consider the edges for the arrays that contribute to the variable "sum" in the computation below.
        # ignore the edges for the error terms as those arrays have been initialized to zero.
        # this is okay because the error term will ultimately not include the edge pixels as they are selected out by the conf_disambig and bitmap arrays.

        i = 0
        for j in range(ny_bh):
            derx_bh[j, i] = ((-3 * bh_mag_vals[j, i]) + (4 * bh_mag_vals[j, i + 1]) - (bh_mag_vals[j, i + 2])) * 0.5

        i = nx_bh - 1
        for j in range(ny_bh):
            derx_bh[j, i] = ((3 * bh_mag_vals[j, i]) + (-4 * bh_mag_vals[j, i - 1]) - (-bh_mag_vals[j, i - 2])) * 0.5

        j = 0
        for i in range(nx_bh):
            dery_bh[j, i] = ((-3 * bh_mag_vals[j, i]) + (4 * bh_mag_vals[j + 1, i]) - (bh_mag_vals[(j + 2), i])) * 0.5

        j = ny_bh - 1
        for i in range(nx_bh):
            dery_bh[j, i] = ((3 * bh_mag_vals[j, i]) + (-4 * bh_mag_vals[j - 1, i]) - (-bh_mag_vals[j - 2, i])) * 0.5

        sum_val = 0.0
        err = 0.0
        count_mask = 0.0
        # Calculate the sum only
        for j in range(1, ny_bh - 1):
            for i in range(1, nx_bh - 1):
                if (derx_bh[j, i] + dery_bh[j, i]) == 0:
                    continue
                sum_val += ma.sqrt(derx_bh[j, i] * derx_bh[j, i] + dery_bh[j, i] * dery_bh[j, i])
                err += err_term2[j, i] / (16.0 * (derx_bh[j, i] * derx_bh[j, i] + dery_bh[j, i] * dery_bh[j, i])) + \
                       err_term1[j, i] / (16.0 * (derx_bh[j, i] * derx_bh[j, i] + dery_bh[j, i] * dery_bh[j, i]))
                count_mask += 1

        mean_derivative_bh = (sum_val) / (count_mask)
        mean_derivative_bh_err = (np.sqrt(err)) / (count_mask)

        cols = ['MEANGBH', 'MEANGBH_ERR']
        data = [[mean_derivative_bh, mean_derivative_bh_err]]
        result = DataFrame(data, columns=cols)
        return result
