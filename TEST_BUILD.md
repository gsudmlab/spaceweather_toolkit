# swdatatoolkit Test Build

If you wish to run this project independently on your local host for test purposes, you will need to set up several
things first.

1. The required libraries listed in the `requirements.txt` file, you should set up a conda environment with the required
   versions of the libraries by first:

       conda create -n swtoolkit

   Then using the `swtoolkit` environment by

       conda activate swtoolkit

   Then install various dependencies

       conda install -c conda-forge sunpy~=5.1.5 pandas~=2.2.3 numpy~=1.26.4 scipy~=1.15.0 scikit-learn~=1.6.0 requests~=2.32.3 aiohttp~=3.11.12 netcdf4~=1.7.2 pytest sphinx

2. Install the library in Dev Mode

   `setuptools` allows you to install a package without copying any files to your interpreter directory (e.g. the
   `site-packages` directory). This allows you to modify your source code and have the changes take effect without you
   having to rebuild and reinstall. Here’s how to do it:

       pip install --editable .

   This creates a link file in your interpreter site package directory which associate with your source code. For more
   information, see ["Development Mode"](https://setuptools.pypa.io/en/latest/userguide/development_mode.html).

***

### Test

1. Tests require the `pytest` module to be installed. They can then be executed with the following command:

       python -m pytest

   To allow print statements to be viewed when testing, add the `-s` argument to the end of the `pytest` call.
   To change the log level to debug when testing, add the `--log-cli-level=DEBUG` argument after the above `-s`.

   Running a specific tests requires you to specify the path to file or directory to run, such as `tests/datasources/`

2. You can run code coverage report recording if you have installed `coverage`

       coverage run --source=py_src -m pytest

   Then print out the report with:

       coverage report -m

*** 

### Make Documentation

1. Making documentation requires you to navigate to the `docs` directory of the project

       cd docs

2. From there you can make and clean the documents

       make html

       make clean

   After that you can navigate to the `docs/_build/html` of the project and serve the html over a
   local web server using:

       python3 -m http.server

   Which should serve things over port `8000`

       Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/)

***

[Return to README](./README.md)

***
***

## Acknowledgment

This project has been supported in part by funding from the Division of Advanced Cyberinfrastructure within the 
Directorate for Computer and Information Science and Engineering, the Division of Atmospheric & Geospace Sciences 
within the Directorate for Geosciences, under NSF awards #1931555 and #1936361. It has also been supported by NASA's 
Space Weather Science Application Research-to-Operations-to-Research program grant #80NSSC22K0272, Multidomain 
Reusable Artificial Intelligence Tools program grant #80NSSC23K1026, and Heliophysics Artificial Intelligence/Machine 
Learning-ready Data program grant #80NSSC24K0238 .

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)  
![alt text](./images/gplv3-88x31.png)

This software is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

***

© 2025 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](http://dmlab.cs.gsu.edu/)

[Georgia State University](http://www.gsu.edu/)