"""
 * swdatatoolkit, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2025 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest

import numpy as np
from PIL import Image

import matplotlib.pyplot as plt

from swdatatoolkit.edgedetection import CannyEdgeDetector


def test_canny_edge_detector_throws_on_low_threshold_lt_zero_in_constructor():
    low_threshold = -1
    high_threshold = 0.2
    kernel_size = 5
    sigma = 1.5
    with pytest.raises(ValueError):
        CannyEdgeDetector(low_threshold, high_threshold, kernel_size, sigma)


def test_canny_edge_detector_throws_on_high_threshold_lt_zero_in_constructor():
    low_threshold = 0.2
    high_threshold = -0.2
    kernel_size = 5
    sigma = 1.5
    with pytest.raises(ValueError):
        CannyEdgeDetector(low_threshold, high_threshold, kernel_size, sigma)


def test_canny_edge_detector_throws_on_sigma_lte_zero_in_constructor():
    low_threshold = 0.2
    high_threshold = 0.2
    kernel_size = 5
    sigma = 0
    with pytest.raises(ValueError):
        CannyEdgeDetector(low_threshold, high_threshold, kernel_size, sigma)


# def test_canny_edge_detector():
#     low_threshold = 0.002
#     high_threshold = 0.08
#     kernel_size = 25
#     sigma = 1.9
#
#     img = Image.open('tests/edgedetection/4.jpg').convert('L')
#     w = img.width
#     h = img.height
#     img_arr = np.array(img.getdata()).reshape((h, w))
#     ed = CannyEdgeDetector(low_threshold, high_threshold, kernel_size, sigma)
#
#     img_canny = ed.get_edges(img_arr)
#
#     plt.figure(figsize=(20, 40))
#     plt.imshow(img_canny)
#     plt.savefig('edg2.jpg')
