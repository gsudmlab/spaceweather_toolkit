"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from datetime import datetime, timedelta

from swdatatoolkit.datasources import OMNIWebDownloader


def test_omni_web_download_five_min():
    downloader = OMNIWebDownloader()

    start_time = datetime(2020, 1, 2, 1, 0, 0)
    end_time = datetime(2020, 1, 2, 2, 0, 0)

    df = downloader.get_five_min_avg_data(start_time, end_time)

    assert ((df.index[0] - start_time) < timedelta(minutes=10))
    assert (len(df.index) == 12)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=10))


def test_omni_web_download_throws_if_before_limit():
    start_time = datetime(1981, 12, 31)
    end_time = datetime(1981, 12, 31, 2, 0, 0)

    downloader = OMNIWebDownloader()

    with pytest.raises(ValueError):
        downloader.get_five_min_avg_data(start_time, end_time)


def test_omni_web_download_throws_if_end_before_start():
    start_time = datetime(1997, 4, 2, 2, 0, 0)
    end_time = datetime(1997, 4, 2, 1, 0, 0)

    downloader = OMNIWebDownloader()

    with pytest.raises(ValueError):
        downloader.get_five_min_avg_data(start_time, end_time)
