"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from datetime import datetime, timedelta

from swdatatoolkit.datasources import SXRDownloader


def test_goes_xrs_download_one_min_now():
    end_time = datetime.now()
    start_time = end_time - timedelta(hours=1)

    downloader = SXRDownloader()

    df = downloader.get_one_minute_avg_data(start_time, end_time)
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_goes_xrs_download_one_min_goes_r():
    end_time = datetime(2017, 3, 1, 2)
    start_time = end_time - timedelta(hours=1)

    downloader = SXRDownloader()

    df = downloader.get_one_minute_avg_data(start_time, end_time)
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_goes_xrs_download_one_min_goes_r_day_transition():
    end_time = datetime(2017, 3, 1, 0, 30)
    start_time = end_time - timedelta(hours=1)

    downloader = SXRDownloader()

    df = downloader.get_one_minute_avg_data(start_time, end_time)
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_goes_xrs_download_one_min_old_data():
    end_time = datetime(2016, 3, 1, 2)
    start_time = end_time - timedelta(hours=1)

    downloader = SXRDownloader()

    df = downloader.get_one_minute_avg_data(start_time, end_time)
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_goes_xrs_download_throws_if_before_limit():
    start_time = datetime(1995, 1, 2, 1, 0, 0)
    end_time = datetime(1995, 1, 2, 2, 0, 0)

    downloader = SXRDownloader()

    with pytest.raises(ValueError):
        downloader.get_one_minute_avg_data(start_time, end_time)


def test_goes_xrs_download_throws_if_end_before_start():
    start_time = datetime(1995, 3, 2, 2, 1, 0, 0)
    end_time = datetime(1995, 3, 2, 2, 0, 0)

    downloader = SXRDownloader()

    with pytest.raises(ValueError):
        downloader.get_one_minute_avg_data(start_time, end_time)
