"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from datetime import datetime

from swdatatoolkit.datasources import DSCOVRDownloader


def test_dscovr_download():
    start_time = datetime(2017, 4, 2, 1, 0, 0)
    end_time = datetime(2017, 4, 2, 3, 0, 0)

    downloader = DSCOVRDownloader()

    downloader.get_fc_data(start_time, end_time)


def test_dscovr_download_throws_if_end_before_start():
    start_time = datetime(2016, 8, 2, 1, 0, 0)
    end_time = datetime(2016, 8, 1, 2, 0, 0)

    downloader = DSCOVRDownloader()

    with pytest.raises(ValueError):
        downloader.get_fc_data(start_time, end_time)


def test_dscovr_download_throws_if_before_limit():
    start_time = datetime(2016, 7, 1, 1, 0, 0)
    end_time = datetime(2016, 7, 1, 2, 0, 0)

    downloader = DSCOVRDownloader()

    with pytest.raises(ValueError):
        downloader.get_fc_data(start_time, end_time)
