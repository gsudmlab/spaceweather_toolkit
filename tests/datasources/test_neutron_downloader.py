"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""

import pytest
from datetime import datetime, timedelta

from swdatatoolkit.datasources import NeutronDownloader


def test_neutron_one_min_oulu():
    start_time = datetime(2014, 3, 15)
    end_time = datetime(2014, 3, 15, 1, 0, 0)

    downloader = NeutronDownloader()

    df = downloader.get_one_min_avg_data(start_time, end_time, 'OULU')
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_neutron_one_min_domb():
    start_time = datetime(2017, 3, 15)
    end_time = datetime(2017, 3, 15, 1, 0, 0)

    downloader = NeutronDownloader()

    df = downloader.get_one_min_avg_data(start_time, end_time, 'DOMB')
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_neutron_one_min_domc():
    start_time = datetime(2017, 3, 15)
    end_time = datetime(2017, 3, 15, 1, 0, 0)

    downloader = NeutronDownloader()

    df = downloader.get_one_min_avg_data(start_time, end_time, 'DOMC')
    assert ((df.index[0] - start_time) < timedelta(minutes=2))
    assert (len(df.index) == 60)
    assert ((df.index[len(df.index) - 1] - end_time) < timedelta(minutes=2))


def test_neutron_download_throws_if_before_limit_default():
    start_time = datetime(2010, 3, 31)
    end_time = datetime(2010, 3, 31, 2, 0, 0)

    downloader = NeutronDownloader()

    with pytest.raises(ValueError):
        downloader.get_one_min_avg_data(start_time, end_time)


def test_neutron_download_throws_if_before_limit_domb():
    start_time = datetime(2016, 3, 11)
    end_time = datetime(2016, 3, 11, 2, 0, 0)

    downloader = NeutronDownloader()

    with pytest.raises(ValueError):
        downloader.get_one_min_avg_data(start_time, end_time, 'DOMB')


def test_neutron_download_throws_if_before_limit_domc():
    start_time = datetime(2016, 3, 11)
    end_time = datetime(2016, 3, 11, 2, 0, 0)

    downloader = NeutronDownloader()

    with pytest.raises(ValueError):
        downloader.get_one_min_avg_data(start_time, end_time, 'DOMC')


def test_neutron_download_throws_if_end_before_start():
    start_time = datetime(2016, 4, 2, 2, 0, 0)
    end_time = datetime(2016, 4, 2, 1, 0, 0)

    downloader = NeutronDownloader()

    with pytest.raises(ValueError):
        downloader.get_one_min_avg_data(start_time, end_time)
