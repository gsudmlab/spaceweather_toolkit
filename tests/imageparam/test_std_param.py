"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy as np
from PIL import Image

from swdatatoolkit.imageparam import PatchSize
from swdatatoolkit.imageparam import StdDeviationParamCalculator


############################################
# Test StdDeviationParamCalculator
############################################
def test_std_dev_calculator_throws_on_null_patch_in_constructor():
    patch_size = None
    with pytest.raises(TypeError):
        StdDeviationParamCalculator(patch_size)


def test_std_dev_throws_on_null_data():
    patch_size = PatchSize.FOUR
    test_obj = StdDeviationParamCalculator(patch_size)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(None)


def test_std_dev_throws_on_wrong_data_type():
    data = [0, 1, 2]
    patch_size = PatchSize.FOUR
    test_obj = StdDeviationParamCalculator(patch_size)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(data)


def test_std_dev_throws_on_wrong_data_width():
    data = np.arange(8).reshape((4, 2))
    patch_size = PatchSize.FOUR
    test_obj = StdDeviationParamCalculator(patch_size)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_std_dev_throws_on_wrong_data_height():
    data = np.arange(8).reshape((2, 4))
    patch_size = PatchSize.FOUR
    test_obj = StdDeviationParamCalculator(patch_size)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_std_dev_full():
    data = [2, 2, 2, 2]
    data = np.reshape(np.array(data), (2, 2))
    patch_size = PatchSize.FULL
    test_obj = StdDeviationParamCalculator(patch_size)
    val = test_obj.calculate_parameter(data)
    assert val == 0


def test_std_dev():
    # data = np.arange(16).reshape((4, 4))
    patch_size = PatchSize.SIXTY_FOUR
    test_obj = StdDeviationParamCalculator(patch_size)

    # Read image as ndarray
    img = Image.open('tests/images/2025_02_21__15_03_45_350__SDO_AIA_AIA_171.jp2')
    w = img.width
    h = img.height
    img_arr = np.array(img.getdata()).reshape((h, w))

    # Select 64x64 pixel section
    data = img_arr[1024:1088, 1024:1088]

    result = np.std(data)
    val = test_obj.calculate_parameter(data)
    assert val[0, 0] == result
