"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy as np
from PIL import Image

from unittest.mock import Mock

from swdatatoolkit.imageparam import PatchSize
from swdatatoolkit.imageparam import FractalDimParamCalculator
from swdatatoolkit.edgedetection import CannyEdgeDetector


########################################
# Test FractalDimParamCalculator
########################################
def test_f_dim_calculator_throws_on_null_patch_in_constructor():
    patch_size = None
    binary_colors = 94
    edge_detector = CannyEdgeDetector(2.5, 7.5, 2, 16)
    with pytest.raises(TypeError):
        FractalDimParamCalculator(patch_size, edge_detector)


def test_f_dim_calculator_throws_on_null_edge_detector_in_constructor():
    patch_size = PatchSize.FOUR
    edge_detector = None
    with pytest.raises(TypeError):
        FractalDimParamCalculator(patch_size, edge_detector)


def test_f_dim_calculator_throws_on_null_data():
    patch_size = PatchSize.FOUR
    edge_detector = Mock(spec=CannyEdgeDetector)

    test_obj = FractalDimParamCalculator(patch_size, edge_detector)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(None)


def test_f_dim_calculator_throws_on_wrong_data_type():
    patch_size = PatchSize.FOUR
    edge_detector = 2

    data = [1, 2, 3, 4]
    test_obj = FractalDimParamCalculator(patch_size, edge_detector)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(data)


def test_f_dim_calculator_throws_on_wrong_data_width():
    patch_size = PatchSize.FOUR
    edge_detector = 2

    data = np.arange(8).reshape((4, 2))
    test_obj = FractalDimParamCalculator(patch_size, edge_detector)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_f_dim_calculator_throws_on_wrong_data_height():
    patch_size = PatchSize.FOUR
    edge_detector = 2

    data = np.arange(8).reshape((2, 4))
    test_obj = FractalDimParamCalculator(patch_size, edge_detector)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_f_dim_calculator():
    low_threshold = 0.002
    high_threshold = 0.08
    kernel_size = 6
    sigma = 1.9
    patch_size = PatchSize.SIXTY_FOUR
    edge_detector = CannyEdgeDetector(low_threshold, high_threshold, kernel_size, sigma)

    # Read image as ndarray
    img = Image.open('tests/images/2025_02_21__15_03_45_350__SDO_AIA_AIA_171.jp2')
    w = img.width
    h = img.height
    img_arr = np.array(img.getdata()).reshape((h, w))

    # Select 64x64 pixel section
    data = img_arr[1408:1600, 3444:3764]

    test_obj = FractalDimParamCalculator(patch_size, edge_detector)

    result = 1.54
    val = test_obj.calculate_parameter(data)
    assert abs(val[0, 0] - result) < 0.01
