"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy as np
from PIL import Image

from swdatatoolkit.imageparam import PatchSize
from swdatatoolkit.imageparam.util import PeakDetector
from swdatatoolkit.edgedetection import GradientCalculator
from swdatatoolkit.imageparam import TDirectionalityParamCalculator


########################################
# Test TDirectionalityCalculator
########################################
def test_t_directionality_calculator_throws_on_null_patch_in_constructor():
    patch_size = None
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = 1
    with pytest.raises(TypeError):
        TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)


def test_t_directionality_throws_on_null_gradient_calculator_in_constructor():
    patch_size = PatchSize.FOUR
    gradient_calculator = None
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = 1
    with pytest.raises(TypeError):
        TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)


def test_t_directionality_throws_on_null_peak_detector_in_constructor():
    patch_size = PatchSize.FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = None
    quantization_level = 1
    with pytest.raises(TypeError):
        TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)


def test_t_directionality_throws_on_null_quantization_level_in_constructor():
    patch_size = PatchSize.FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = None
    with pytest.raises(TypeError):
        TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)


def test_t_directionality_throws_on_null_data():
    patch_size = PatchSize.FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = 1
    test_obj = TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(None)


def test_t_directionality_throws_on_wrong_data_type():
    patch_size = PatchSize.FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = 1

    data = [1, 2, 3, 4]
    test_obj = TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(data)


def test_t_directionality_throws_on_wrong_data_width():
    patch_size = PatchSize.FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = 1

    data = np.arange(8).reshape((4, 2))
    test_obj = TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_t_directionality_throws_on_wrong_data_height():
    patch_size = PatchSize.FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(1, 25, 0, True)
    quantization_level = 1

    data = np.arange(8).reshape((2, 4))
    test_obj = TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_t_directionality():
    patch_size = PatchSize.SIXTY_FOUR
    gradient_calculator = GradientCalculator('sobel')
    peak_detector = PeakDetector(length=1, threshold=25, is_percentile=False)
    quantization_level = 90

    test_obj = TDirectionalityParamCalculator(patch_size, gradient_calculator, peak_detector, quantization_level)

    # Read image as ndarray
    img = Image.open('tests/images/2025_02_21__15_03_45_350__SDO_AIA_AIA_171.jp2')
    w = img.width
    h = img.height
    img_arr = np.array(img.getdata()).reshape((h, w))

    # Select 64x64 pixel section
    data = img_arr[1472:1536, 3508:3572]

    result = 54.4
    val = test_obj.calculate_parameter(data)
    assert abs(val[0, 0] - result) < 0.05
