"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2025 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy as np
from PIL import Image

from swdatatoolkit.imageparam import PatchSize
from swdatatoolkit.imageparam import EntropyParamCalculator


#######################################
# Test EntropyParamCalculator
#######################################
def test_entropy_calculator_throws_on_null_patch_in_constructor():
    patch_size = None
    n_bins = 1
    min_val = 0
    max_val = 1
    with pytest.raises(TypeError):
        EntropyParamCalculator(patch_size, n_bins, min_val, max_val)


def test_entropy_calculator_throws_on_null_min_val_in_constructor():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = None
    max_val = 1
    with pytest.raises(TypeError):
        EntropyParamCalculator(patch_size, n_bins, min_val, max_val)


def test_entropy_calculator_throws_on_null_max_val_in_constructor():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = 0
    max_val = None
    with pytest.raises(TypeError):
        EntropyParamCalculator(patch_size, n_bins, min_val, max_val)


def test_entropy_calculator_throws_on_max_val_less_than_min_in_constructor():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = 3
    max_val = 1
    with pytest.raises(ValueError):
        EntropyParamCalculator(patch_size, n_bins, min_val, max_val)


def test_entropy_calculator_throws_on_null_data():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = 0
    max_val = 10
    test_obj = EntropyParamCalculator(patch_size, n_bins, min_val, max_val)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(None)


def test_entropy_calculator_throws_on_data_wrong_type():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = 0
    max_val = 10

    data = [1, 2, 3, 4]
    test_obj = EntropyParamCalculator(patch_size, n_bins, min_val, max_val)
    with pytest.raises(TypeError):
        test_obj.calculate_parameter(data)


def test_entropy_throws_on_wrong_data_width():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = 0
    max_val = 10

    data = np.arange(8).reshape((4, 2))
    test_obj = EntropyParamCalculator(patch_size, n_bins, min_val, max_val)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_entropy_throws_on_wrong_data_height():
    patch_size = PatchSize.FOUR
    n_bins = 5
    min_val = 0
    max_val = 10

    data = np.arange(8).reshape((2, 4))
    test_obj = EntropyParamCalculator(patch_size, n_bins, min_val, max_val)
    with pytest.raises(ValueError):
        test_obj.calculate_parameter(data)


def test_entropy():
    patch_size = PatchSize.SIXTY_FOUR
    n_bins = 60
    min_val = 0
    max_val = 255

    # Read image as ndarray
    img = Image.open('tests/images/2025_02_21__15_03_45_350__SDO_AIA_AIA_171.jp2').convert('L')
    w = img.width
    h = img.height
    img_arr = np.array(img.getdata()).reshape((h, w))

    # Select 64x64 pixel section
    data = img_arr[1472:1536, 3508:3572]
    test_obj = EntropyParamCalculator(patch_size, n_bins, min_val, max_val)

    result = 4.52
    val = test_obj.calculate_parameter(data)
    assert abs(val[0, 0] - result) < 0.01
