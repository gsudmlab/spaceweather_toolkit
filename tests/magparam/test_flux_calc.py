"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy.ma as ma

from swdatatoolkit.magparam import FluxCalculator


def test_flux_calculator_throws_on_none_for_bz_mag_array():
    bz_mag_vals = None
    bz_err_vals = ma.ones((2, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(TypeError):
        calculator = FluxCalculator()
        calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_flux_calculator_throws_on_none_for_bz_err_array():
    bz_mag_vals = ma.ones((2, 2))
    bz_err_vals = None
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(TypeError):
        calculator = FluxCalculator()
        calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_flux_calculator_throws_on_wrong_y_size_bz_err_array():
    bz_mag_vals = ma.ones((2, 2))
    bz_err_vals = ma.ones((1, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = FluxCalculator()
        calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_flux_calculator_throws_on_wrong_y_size_bz_array():
    bz_mag_vals = ma.ones((1, 2))
    bz_err_vals = ma.ones((2, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = FluxCalculator()
        calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_flux_calculator_throws_on_wrong_x_size_bz_err_array():
    bz_mag_vals = ma.ones((2, 2))
    bz_err_vals = ma.ones((2, 1))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = FluxCalculator()
        calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_flux_calculator_throws_on_wrong_x_size_bz_array():
    bz_mag_vals = ma.ones((2, 1))
    bz_err_vals = ma.ones((2, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = FluxCalculator()
        calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_flux_calculator_correct_result_usflux_and_err_values():
    bz_mag_vals = ma.ones((1, 1))
    bz_mag_vals[0, 0] = 0.5
    bz_err_vals = ma.ones((1, 1))
    bz_err_vals[0, 0] = 0.25
    cdelt1 = 0.5
    rsun_ref = 10.0
    rsun_obs = 5.0

    calculator = FluxCalculator()
    result = calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)

    assert (result.iloc[0]['USFLUX'] == 5000)
    assert (result.iloc[0]['USFLUX_ERR'] == 2500)


def test_flux_calculator_correct_result_fluximb_value():
    bz_mag_vals = ma.zeros((2, 2))
    bz_mag_vals[0, 0] = 1.0
    bz_mag_vals[0, 1] = -0.5
    bz_err_vals = ma.zeros((2, 2))
    cdelt1 = 0.5
    rsun_ref = 10.0
    rsun_obs = 5.0

    calculator = FluxCalculator()
    result = calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)

    assert (result.iloc[0]['FLUXIMB'] == 5000)


def test_flux_calculator_correct_result_values_w_mask():
    bz_mag_vals = ma.zeros((2, 2))
    bz_mag_vals[0, 0] = 1.0
    bz_mag_vals[0, 1] = -0.5
    bz_mag_vals[1, 0] = ma.masked

    bz_err_vals = ma.zeros((2, 2))
    bz_err_vals[0, 0] = 0.25
    bz_err_vals[1, 0] = 0.25
    bz_err_vals[1, 0] = ma.masked

    cdelt1 = 0.5
    rsun_ref = 10.0
    rsun_obs = 5.0

    calculator = FluxCalculator()
    result = calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)

    assert (result.iloc[0]['USFLUX'] == 15000)
    assert (result.iloc[0]['USFLUX_ERR'] == 2500)
    assert (result.iloc[0]['FLUXIMB'] == 5000)
