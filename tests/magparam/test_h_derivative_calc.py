"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy.ma as ma

from swdatatoolkit.magparam import HorizontalDerivativeCalculator


def test_h_derive_calculator_throws_on_none_for_bh_mag_array():
    bh_mag_vals = None
    bh_err_vals = ma.ones((2, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(TypeError):
        calculator = HorizontalDerivativeCalculator()
        calculator.calc(bh_mag_vals, bh_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_h_derive_calculator_throws_on_none_for_bh_mag_err_array():
    bh_mag_vals = ma.ones((2, 2))
    bh_err_vals = None
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(TypeError):
        calculator = HorizontalDerivativeCalculator()
        calculator.calc(bh_mag_vals, bh_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_h_derive_calculator_throws_on_wrong_y_size_bh_err_array():
    bh_mag_vals = ma.ones((2, 2))
    bh_err_vals = ma.ones((1, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = HorizontalDerivativeCalculator()
        calculator.calc(bh_mag_vals, bh_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_h_derive_calculator_throws_on_wrong_x_size_bh_err_array():
    bh_mag_vals = ma.ones((2, 2))
    bh_err_vals = ma.ones((2, 1))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = HorizontalDerivativeCalculator()
        calculator.calc(bh_mag_vals, bh_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_h_derive_calculator_throws_on_wrong_y_size_bh_mag_array():
    bh_mag_vals = ma.ones((1, 2))
    bh_err_vals = ma.ones((2, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = HorizontalDerivativeCalculator()
        calculator.calc(bh_mag_vals, bh_err_vals, cdelt1, rsun_ref, rsun_obs)


def test_h_derive_calculator_throws_on_wrong_x_size_bh_mag_array():
    bh_mag_vals = ma.ones((2, 1))
    bh_err_vals = ma.ones((2, 2))
    cdelt1 = 1.0
    rsun_ref = 1.0
    rsun_obs = 1.0
    with pytest.raises(ValueError):
        calculator = HorizontalDerivativeCalculator()
        calculator.calc(bh_mag_vals, bh_err_vals, cdelt1, rsun_ref, rsun_obs)


# def test_h_derive_calculator_correct_result_meangbh_and_err_values():
#     bz_mag_vals = ma.ones((3, 3))
#     bz_mag_vals[1, 1] = 0.5
#     bz_mag_vals[0, 0] = ma.masked
#     bz_err_vals = ma.ones((3, 3))
#     bz_err_vals[0, 0] = 0.25
#     cdelt1 = 0.5
#     rsun_ref = 10.0
#     rsun_obs = 5.0
#
#     calculator = HorizontalDerivativeCalculator()
#     result = calculator.calc(bz_mag_vals, bz_err_vals, cdelt1, rsun_ref, rsun_obs)
#     print(result)
#     # assert (result.iloc[0]['USFLUX'] == 5000)
#     # assert (result.iloc[0]['USFLUX_ERR'] == 2500)
