"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy as np

from swdatatoolkit.magparam import HorizontalFieldCalculator


def test_h_field_calculator_throws_on_none_for_bx_mag_array():
    bx_mag_vals = None
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(TypeError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_none_for_by_mag_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = None
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(TypeError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_none_for_bx_err_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = None
    by_err = np.zeros((2, 2))
    with pytest.raises(TypeError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_none_for_by_err_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 2))
    by_err = None
    with pytest.raises(TypeError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_y_size_for_bx_mag_array():
    bx_mag_vals = np.zeros((1, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_y_size_for_by_mag_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.zeros((1, 2))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_y_size_for_bx_err_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((1, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_y_size_for_by_err_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((1, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_x_size_for_bx_mag_array():
    bx_mag_vals = np.zeros((2, 1))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_x_size_for_by_mag_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.zeros((2, 1))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_x_size_for_bx_err_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 1))
    by_err = np.zeros((2, 2))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_throws_on_wrong_x_size_for_by_err_array():
    bx_mag_vals = np.ones((2, 2))
    by_mag_vals = np.ones((2, 2))
    bx_err = np.zeros((2, 2))
    by_err = np.zeros((2, 1))
    with pytest.raises(ValueError):
        calculator = HorizontalFieldCalculator()
        calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)


def test_h_field_calculator_correct_values():
    bx_mag_vals = np.zeros((1, 1))
    bx_mag_vals[0, 0] = 1.5
    by_mag_vals = np.zeros((1, 1))
    by_mag_vals[0, 0] = 2
    bx_err = np.zeros((1, 1))
    bx_err[0, 0] = 0.5
    by_err = np.zeros((1, 1))
    by_err[0, 0] = 0.25

    calculator = HorizontalFieldCalculator()
    bh, bh_err = calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)
    assert (bh[0, 0] == 2.5)
    assert (np.fabs(bh_err[0, 0] - 0.36055) < 0.001)


def test_h_field_calculator_correct_values_w_nans():
    bx_mag_vals = np.zeros((2, 2))
    bx_mag_vals[0, 0] = 1.5
    bx_mag_vals[0, 1] = np.nan
    bx_mag_vals[1, 0] = 1.5
    by_mag_vals = np.zeros((2, 2))
    by_mag_vals[0, 0] = 2
    by_mag_vals[0, 1] = 2
    by_mag_vals[1, 0] = np.nan
    bx_err = np.zeros((2, 2))
    bx_err[0, 0] = 0.5
    bx_err[0, 0] = 0.5
    by_err = np.zeros((2, 2))
    by_err[0, 0] = 0.25
    by_err[0, 1] = 0.25

    calculator = HorizontalFieldCalculator()
    bh, bh_err = calculator.calc(bx_mag_vals, by_mag_vals, bx_err, by_err)
    assert (bh[0, 0] == 2.5)
    assert (bh.mask[0, 1])
    assert (bh.mask[1, 0])

    assert (np.fabs(bh_err[0, 0] - 0.36055) < 0.001)
    assert (bh_err.mask[0, 1])
    assert (bh_err.mask[1, 0])
