"""
 swdatatoolkit, a project at the Data Mining Lab
 (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).

 Copyright (C) 2024 Georgia State University

 This program is free software: you can redistribute it and/or modify it under
 the terms of the GNU General Public License as published by the Free Software
 Foundation version 3.

 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 details.

 You should have received a copy of the GNU General Public License along with
 this program. If not, see <http://www.gnu.org/licenses/>.
"""
import pytest
import numpy as np
import numpy.ma as ma

from swdatatoolkit.magparam import GammaCalculator


def test_gamma_calculator_throws_on_none_for_bz_mag_array():
    bz_mag_vals = None
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(TypeError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_none_for_bh_mag_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = None
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(TypeError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_none_for_bz_err_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.zeros((2, 2))
    bz_err = None
    bh_err = np.zeros((2, 2))

    with pytest.raises(TypeError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_none_for_bh_err_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.zeros((2, 2))
    bz_err = np.zeros((2, 2))
    bh_err = None

    with pytest.raises(TypeError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_y_size_for_bz_mag_array():
    bz_mag_vals = np.ones((1, 2))
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_y_size_for_bh_mag_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.ones((1, 2))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_y_size_for_bz_err_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((1, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_y_size_for_bh_err_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((1, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)



def test_gamma_calculator_throws_on_wrong_x_size_for_bz_mag_array():
    bz_mag_vals = np.ones((2, 1))
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_x_size_for_bh_mag_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.ones((2, 1))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_x_size_for_bz_err_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((2, 1))
    bh_err = np.zeros((2, 2))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_throws_on_wrong_x_size_for_bh_err_array():
    bz_mag_vals = np.ones((2, 2))
    bh_mag_vals = np.ones((2, 2))
    bz_err = np.zeros((2, 2))
    bh_err = np.zeros((2, 1))

    with pytest.raises(ValueError):
        calculator = GammaCalculator()
        calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)


def test_gamma_calculator_correct_result_values():
    bz_mag_vals = ma.ones((1, 1))
    bz_mag_vals[0, 0] = -2

    bz_err = ma.zeros((1, 1))
    bz_err[0, 0] = 2

    bh_mag_vals = ma.ones((1, 1))
    bh_mag_vals[0, 0] = 100

    bh_err = ma.zeros((1, 1))
    bh_err[0, 0] = 2

    calculator = GammaCalculator()
    result = calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)

    assert (np.fabs(result.iloc[0]['MEANGAM'] - 88.854) < 0.001)
    assert (np.fabs(result.iloc[0]['MEANGAM_ERR'] - 1.1457) < 0.001)


def test_gamma_calculator_correct_result_values_w_bh_mask():
    bz_mag_vals = ma.ones((2, 2))
    bz_mag_vals[0, 0] = -2
    bz_err = ma.zeros((2, 2))
    bz_err[0, 0] = 2

    mask_vals = np.ones((2, 2))
    mask_vals[0, 0] = 0

    bh_mag_vals = np.ones((2, 2))
    bh_mag_vals[0, 0] = 100
    bh_mag_vals = ma.masked_array(bh_mag_vals, mask=mask_vals)
    bh_err = np.zeros((2, 2))
    bh_err[0, 0] = 2
    bh_err = ma.masked_array(bh_err, mask=mask_vals)

    calculator = GammaCalculator()
    result = calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)

    assert (np.fabs(result.iloc[0]['MEANGAM'] - 88.854) < 0.001)
    assert (np.fabs(result.iloc[0]['MEANGAM_ERR'] - 1.1457) < 0.001)


def test_gamma_calculator_correct_result_values_w_bz_mask():
    bz_mag_vals = ma.ones((2, 2))
    bz_mag_vals[0, 0] = -2
    bz_mag_vals[0, 1] = ma.masked
    bz_mag_vals[1, 0] = ma.masked
    bz_mag_vals[1, 1] = ma.masked
    bz_err = ma.zeros((2, 2))
    bz_err[0, 0] = 2

    mask_vals = ma.zeros((2, 2))

    bh_mag_vals = np.zeros((2, 2))
    bh_mag_vals[0, 0] = 100
    bh_mag_vals[0, 1] = 100
    bh_mag_vals[1, 0] = 100
    bh_mag_vals[1, 1] = 100
    bh_mag_vals = ma.masked_array(bh_mag_vals, mask=mask_vals)
    bh_err = np.zeros((2, 2))
    bh_err[0, 0] = 2
    bh_err = ma.masked_array(bh_err, mask=mask_vals)

    calculator = GammaCalculator()
    result = calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)

    assert (np.fabs(result.iloc[0]['MEANGAM'] - 88.854) < 0.001)
    assert (np.fabs(result.iloc[0]['MEANGAM_ERR'] - 1.1457) < 0.001)


def test_gamma_calculator_correct_result_values_w_bh_low():
    bz_mag_vals = ma.ones((2, 2))
    bz_mag_vals[0, 0] = -2
    bz_mag_vals[0, 1] = -2
    bz_mag_vals[1, 0] = -2
    bz_mag_vals[1, 1] = -2
    bz_err = ma.zeros((2, 2))
    bz_err[0, 0] = 2
    mask_vals = np.zeros((2, 2))

    bh_mag_vals = np.zeros((2, 2))
    bh_mag_vals[0, 0] = 100
    bh_mag_vals[0, 1] = 99
    bh_mag_vals[1, 0] = 99
    bh_mag_vals[1, 1] = 99
    bh_mag_vals = ma.masked_array(bh_mag_vals, mask=mask_vals)
    bh_err = np.zeros((2, 2))
    bh_err[0, 0] = 2
    bh_err = ma.masked_array(bh_err, mask=mask_vals)

    calculator = GammaCalculator()
    result = calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)

    assert (np.fabs(result.iloc[0]['MEANGAM'] - 88.854) < 0.001)
    assert (np.fabs(result.iloc[0]['MEANGAM_ERR'] - 1.1457) < 0.001)


def test_gamma_calculator_correct_result_values_w_bz_zero():
    bz_mag_vals = ma.zeros((2, 2))
    bz_mag_vals[0, 0] = -2

    bz_err = ma.zeros((2, 2))
    bz_err[0, 0] = 2
    mask_vals = np.zeros((2, 2))

    bh_mag_vals = np.zeros((2, 2))
    bh_mag_vals[0, 0] = 100
    bh_mag_vals[0, 1] = 100
    bh_mag_vals[1, 0] = 100
    bh_mag_vals[1, 1] = 100
    bh_mag_vals = ma.masked_array(bh_mag_vals, mask=mask_vals)
    bh_err = np.zeros((2, 2))
    bh_err[0, 0] = 2
    bh_err = ma.masked_array(bh_err, mask=mask_vals)

    calculator = GammaCalculator()
    result = calculator.calc(bz_mag_vals, bh_mag_vals, bz_err, bh_err)

    assert (np.fabs(result.iloc[0]['MEANGAM'] - 88.854) < 0.001)
    assert (np.fabs(result.iloc[0]['MEANGAM_ERR'] - 1.1457) < 0.001)
