magparam module
=================

.. automodule:: swdatatoolkit.magparam
   :members:
   :undoc-members:
   :show-inheritance:
   :special-members: __init__