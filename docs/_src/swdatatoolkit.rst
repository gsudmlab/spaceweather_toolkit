swdatatoolkit
=============

.. toctree::
   :maxdepth: 3

   imageparam
   datasources
   edgedetection
   magparam